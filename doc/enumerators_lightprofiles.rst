.. _rst_enumerators_lightprofiles:

.. role::  raw-html(raw)
    :format: html

Light profiles
--------------------------------------------------------------

Possible keyword choices for the light profile to be set in the file :option:`gLIST_HALOES_JEANS` (no named variable):

:raw-html:`<center><small>`

.. csv-table:: 
   :file: tables/gENUM_LIGHTPROFILE.csv_table
   :widths: 15, 50, 10, 40, 10, 40

:raw-html:`</small></center><br>&nbsp;<br>`

.. seealso::
   :numref:`rst_physics_jeans`, :ref:`rst_physics_jeans`.
