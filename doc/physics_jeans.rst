.. _rst_physics_jeans:

.. sectionauthor:: David Maurin <dmaurin@lpsc.in2p3.fr>

.. role::  raw-html(raw)
    :format: html

Jeans analysis (jeans_analysis.h)
------------------------------------

The Jeans equation relates the dark matter content of an object (potential well) to its light content (dynamics in the potential). The main steps and ingredients to derive the Jeans equation are recalled below. We recall that the user has to provide the data for the velocity dispersion data (:math:`\sigma_p^2(R)`) and the parameters for the light profile, :math:`I(r)`, :numref:`tab_light_profile`, and the anisotropy profile, :math:`\beta_{\rm ani}(r)`, :numref:`tab_anisotropy_profile`. Jeans analysis calculations are implemented in the library `jeans_analysis.h <doxygen/jeans__analysis_8h.html>`_ in :program:`CLUMPY`.


A reference book for stellar dynamics is :raw-html:`<A href="http://adsabs.harvard.edu/abs/2008gady.book.....B" target="_blank"> Binney and Tremaine's Galactic Dynamics</A>`, where the Jeans equation is described --- see also :raw-html:`<A href="http://cdsads.u-strasbg.fr/abs/2013NewAR..57...52B" target="_blank"> Battaglia et al. (2013)</A>` and :raw-html:`<A href="http://cdsads.u-strasbg.fr/abs/2014RvMP...86...47C" target="_blank"> Courteau et al. (2014)</A>` for reviews on dynamics in the context of dSphs and the Galaxy respectively.

.. warning:: All the expressions below are valid for the spherical-halo case only.


Jeans equation
~~~~~~~~~~~~~~

Assuming virial equilibrium and a non-rotating system, the Jeans equation is obtained by

   1. Using the collisionless Boltzmann equation for the 6D space-phase of stellar density :math:`f(t,\vec{x},\vec{v})`;
   2. Assuming steady-state and spherical symmetry;
   3. Extracting and combining the first two moments :math:`\int\!\!\int\!\!\int f \,v_i^n\, d^3\vec{v}` for :math:`n=0` (density of stars) and :math:`n=1` (mean stellar velocity along the :math:`i`-th component).

We can then link the enclosed mass :math:`M(r)` in radius :math:`r` and moments of the stellar distribution:

   .. math::

      \frac{1}{\nu}\frac{\mathrm{d}}{\mathrm{d}r}(\nu \bar{v_r^2})+2\frac{\beta_{\rm ani}(r)\bar{v_r^2}}{r}=-\frac{GM(r)}{r^2} \quad\quad {\rm [Jeans~Equation]},

with

   - :math:`\nu(r)`, also denoted :math:`\rho(r)`: spatial density (spherical symmetry) of the light profile;
   - :math:`\bar{v_r^2}(r)`: radial velocity dispersion of baryons (e.g. stars in dSphs, or galaxies in galaxy clusters);
   - :math:`\beta_{\rm ani}(r)\equiv 1-\bar{v_{\theta}^2}(r)/\bar{v_r^2}(r)`: orbital anisotropy of the stellar component (depending on the radial :math:`v_r^2` and tangential :math:`v_{\theta}^2` velocity dispersion).


Note that higher orders of the Jeans equation can be derived (integrating on higher moments of the distribution function :math:`f`), but it is not considered in this version of :program:`CLUMPY`.


(De-)projection & Abel
~~~~~~~~~~~~~~~~~~~~~~

In practice, we only have access to projected quantities on the sky, as illustrated in :numref:`fig_proj`.

   .. _fig_proj:

   .. figure:: DocImages/abel_transform.png
      :figclass: align-center
      :scale: 85%

      Framework for Abel transform (projection).

For spherically symmetric systems, projection and de-projection are given by the Abel and inverse Abel transform (the quantity :math:`R` is the projected radius, and :math:`y=\sqrt{r^2-R^2}`, see :numref:`fig_proj`):

   - *Projection*: :math:`f(r)\rightarrow F(R)`

      .. math::

         F(R) = 2 \int_R^\infty \frac{f(r)\,r\,\mathrm{d}r}{\sqrt{r^2-R^2}} = 2\int_0^\infty f(y) \, \mathrm{d}y

      using

      .. math::

         y=\sqrt{r^2-R^2}\rightarrow \mathrm{d}y=\frac{r\,\mathrm{d}r}{\sqrt{r^2-R^2}}.

   - *De-projection*: :math:`F(R)\rightarrow f(r)~~~~~~~~[` valid if :math:`f(r)` drops to zero more quickly than :math:`1/r]`

      .. math::

         f(r) = -\frac{1}{\pi} \int_r^\infty \frac{\mathrm{d}F}{\mathrm{d}R}(R) \times \frac{\mathrm{d}R}{\sqrt{R^2-r^2}} = -\frac{1}{\pi} \int_0^\infty \frac{\mathrm{d}F}{\mathrm{d}R}(R=\sqrt{(Y^2+r^2)}) \times \frac{\mathrm{d}Y}{R}

      using

      .. math::

         Y=\sqrt{R^2-r^2}\rightarrow \mathrm{d}Y=\frac{R\,\mathrm{d}R}{\sqrt{R^2-r^2}}.

In practice, because of the singularity of the integrand for :math:`r=R`, the integration better behaves numerically (faster convergence) if it is performed over the variable :math:`y` (for projection, see :numref:`fig_proj`) or :math:`Y` (for de-projection): this is what we use in :program:`CLUMPY`.


Formal solutions
~~~~~~~~~~~~~~~~

We can write the formal solution of the Jeans equation thanks to :math:`\nu(r)\bar{v_r^2}(r)` and :math:`I(R)\sigma_p^2(R)`.

**Solving for deprojected (3D)**: :math:`\nu(r)\bar{v_r^2}(r)`

   The above Jeans equation is a first order linear non-homogeneous differential equation with non constant coefficients. Its general solution is given for instance :raw-html:`<a href="http://mathworld.wolfram.com/First-OrderOrdinaryDifferentialEquation.html" target="_blank">here</A>`, and for the Jeans equation this gives:

      .. math::
         \nu(r)\bar{v_r^2}(r) = \frac{1}{f(r)}\times \int_r^\infty f(s) \nu(s) \frac{GM(s)}{s^2}\, \mathrm{d}s
         :label: eq_jeans1 

      with

      .. math::
         f(r) = f_{r_1} \exp\left[\int_{r_1}^r \frac{2}{t}\beta_{\rm ani}(t)\, \mathrm{d}t \right].
         :label: eq_jeans2 

   The integration on the lower boundary :math:`r_1` leads to a normalisation factor that disappears in the solution :math:`\nu(r)\bar{v_r^2}(r)`, because the function :math:`f` appears both in the numerator and denominator.


**Solving for projected (2D)**: :math:`I(R)\sigma_p^2(R)`

   To compare the velocity dispersion to data, one needs to project the above equation (only line-of-sight projected velocity dispersion are available to the observer). Using the Abel transform, we have:

      .. math::
         I(R)\sigma_p^2(R)&=2\displaystyle \int_{R}^{\infty}\biggl (1-\beta_{\rm ani}(r)\frac{R^2}{r^2}\biggr )
          \frac{\nu(r) \,\bar{v_r^2}(r)\,r}{\sqrt{r^2-R^2}}\mathrm{d}r\\
         &= 2\displaystyle \int_{0}^{\infty}\biggl (1-\beta_{\rm ani}(r)\frac{R^2}{r^2}\biggr )
          \nu(r) \,\bar{v_r^2}(r)\mathrm{d}y .
         :label: eq_jeans3 

   using the change of variable :math:`y=\sqrt{r^2-R^2}`, which gives :math:`dy=r\,dr/\sqrt{r^2-R^2}`.

   Depending on the spatial distribution for the anisotropy :math:`\beta_{\rm ani}(r)`, the above equations tell us that three integrations are required (which is quite time consumming): one to get :math:`f(r)`, one to get :math:`\nu(r)\bar{v_r^2}(r)`, and one last to get :math:`I(R)\sigma_p^2(R)`. In practice, there are several special cases for which:

      - :math:`f(r)` has an analytical form, reducing the number of integration to two --- one for :math:`\nu(r)\bar{v_r^2}(r)` and one for :math:`I(R)\sigma_p^2(R)`;
      - relying on the so-called Kernel :math:`{\cal K}` (:raw-html:`<A href="http://cdsads.u-strasbg.fr/abs/2005MNRAS.363..705M" target="_blank">Mamon and Lokas, 2005</A>`), the solution can be rewritten as a single integration:

         .. math::
            I(R)\sigma_p^2(R)=2 G\times \int_{R}^{\infty} {\cal K}\left(\frac{r}{R},\,\frac{r_a}{R}\right) \nu(r) M(r) \frac{\mathrm{d}r}{r}.
            :label: eq_jeans4

   We present the anisotropy profiles used and the corresponding solutions for :math:`f(r)` and/or  :math:`I(R)\sigma_p^2(R)` below.



Inputs in :program:`CLUMPY`
~~~~~~~~~~~~~~~~~~~~~~~~~~~

From the above equations, four ingredients are required to perform the Jeans analysis. They are described below along with the parametrisations available in :program:`CLUMPY`.

**1. Dark matter density profile** :math:`\rho_{\rm DM}` **(and total mass)**

   Any of the profiles discussed in :numref:`rst_physics_profiles` can be used. Note that the baryonic mass should enter the total mass in the calculation in principle, but it is neglected in dSphs, where the mass-to-light ratio is of the order of hundreds.


**2. Surface brightness profiles** :math:`I(R)\equiv\Sigma(R)` **and its deprojection** :math:`\nu(r)\equiv\rho(r)`

   Several light profiles are routinely used in the literature (Plummer, King, Sérsic...). They are implemented in :program:`CLUMPY`. These profiles have from 2 to 5 free parameters (normalisation, scale radius, plus some structural parameters). A difficulty is to decide whether the analytical form used for the profile should be assumed for the surface brightness (projected 2D profile) or for the density distribution (un-projected 3D profile). In the first case, de-projection is required (inverse Abel transform), whereas in the second case, projection is (Abel transform). The extension 2D or 3D is added at the end of the profile keyword to explicitly indicate which case it is, as listed in :numref:`tab_light_profile`.

:raw-html:`<center><small>`

.. _tab_light_profile:

.. csv-table:: Global enumerators available in :option:`gENUM_LIGHTPROFILE` (for exponential profiles, :math:`K_0` and :math:`K_1` are respectively the modified Bessel function of the second kind of order 0 and 1).
   :file: tables/gENUM_LIGHTPROFILE.csv_table
   :widths: 15, 50, 10, 40, 10, 40
   
:raw-html:`</small></center><br>&nbsp;<br>`


**3. Anisotropy profile** :math:`\beta_{\rm ani}(r)`

   We recall that the anisotropy profile is given by a combination of the radial and tangential velocity dispersion (and that :math:`\beta_{\rm ani}(r)<1` for any :math:`r`):

   .. math::

         \beta_{\rm ani}(r)\equiv 1-\frac{\bar{v_{\theta}^2}(r)}{\bar{v_r^2}(r)}\,.

   Due to the lack of information on the anisotropy profile, the first profiles discussed in the literature were based on analytical studies to build dynamical models (in spherical symmetry) having realistic anisotropy profile. Very few of these models are known: the ones considered are either completely isotropic (:math:`\beta_{\rm ani}=0`), or have a constant anisotropy or an anisotropy profile of Osipkov-Merritt type (isotropic in the centre and completely radially anisotropic at large radii). More recently, indications of radial anisotropy in the outer regions of dark matter haloes have been obtained from numerical simulations. In the inner region, dynamical formation and evolution processes can lead to a strong anisotropy. To better describe these profiles, :raw-html:`<a href="http://adsabs.harvard.edu/abs/2007A%26A...471..419B" target="_blank"> Baes and van Hese (2007)</a>` introduced a technique to construct dynamical models with an arbitrary density and an arbitrary anisotropy profile.

   We gather in :numref:`tab_anisotropy_profile` these various profile and the function :math:`f(r)` and kernel :math:`{\cal K}(u,u_a)` required to solve Jeans Eqs :eq:`eq_jeans1` and :eq:`eq_jeans4`, i.e.

   .. math::

      \nu(r)\bar{v_r^2}(r) = \frac{1}{f(r)}\times \int_r^\infty f(s) \nu(s) \frac{GM(s)}{s^2}\, \mathrm{d}s

   and

   .. math::

      I(R)\sigma_p^2(R)=2 G\times \int_{R}^{\infty} {\cal K}\left(\frac{r}{R},\,\frac{r_a}{R}\right) \nu(r) M(r) \frac{\mathrm{d}r}{r},


:raw-html:`<center><small>`

.. _tab_anisotropy_profile:

.. csv-table:: Global enumerators available in :option:`gNAMES_ANISOTROPYPROFILE` (:keyword:`kOSIPKOV` is a special case of :keyword:`kBAES` with :math:`\beta_0=0`, :math:`\beta_\infty=1`, and :math:`\eta=2`).
   :file: tables/gENUM_ANISOTROPYPROFILE.csv_table
   :widths: 10, 30,30,30,30
   
:raw-html:`</small></center>`


Sketch of Jeans run
~~~~~~~~~~~~~~~~~~~

:numref:`fig_jeans_run` gives a sketch on how to proceed with a Jeans analysis, with the inputs and outputs.

   .. _fig_jeans_run:

   .. figure:: DocImages/Diagram.png
      :align: center
      :figclass: align-center
      :scale: 55%

      Diagram of the Jeans analysis with :program:`CLUMPY`. From a kinematic data file and a parameter file describing the free parameters, a statistical Jeans analysis can be run with :math:`{\tt clumpy\_jeansMCMC}` or :math:`{\tt clumpy\_jeansChi2}`. A *statistical* output file is created, from which estimators of different quantities of interest (i.e., *J*-factors) can be obtained with :math:`{\tt clumpy -s}`.


Log-likelihood (for fit)
~~~~~~~~~~~~~~~~~~~~~~~~

To obtain the best-fit DM halo parameters solving the Jeans equation (see :numref:`rst_doc_module_jeansChi2`), we can rely on several likelihood functions. The details can be found in :raw-html:`<A href="http://adsabs.harvard.edu/abs/2015arXiv150402048B," target="_blank">Bonnivard et al. (2015)</A>`.


**Binned and unbinned analysis**

   For the binned analysis, the velocity dispersion profiles :math:`\sigma_{\rm obs}(R)` are built from the individual stellar velocities and the likelihood function is:

   .. math::

     \mathcal{L}^{\rm bin}= \prod_{i=1}^{N_{\rm bins}}
     \frac{(2\pi)^{-1/2}}{\Delta{\sigma_i}(R_i)} \exp\biggl
            [-\frac{1}{2}\biggl (\frac{\sigma_{\rm obs}(R_{i})
                -\sigma_{p}(R_i)}{\Delta {\sigma}_i(R_i)}\biggr
              )^{2}\biggr ].

   The quantity :math:`\Delta {\sigma_{\rm i}}(R_i)` is the error on the velocity dispersion at the radius :math:`R_i`.

   For the unbinned analysis, we assume that the distribution of line-of-sight stellar velocities is Gaussian, centred on the mean stellar velocity :math:`\bar{v}`:

   .. math::

     \mathcal{L}^{\rm unbin}= \prod_{i=1}^{N_{\rm stars}}
       \frac{(2\pi)^{-1/2}}{\sqrt{\sigma_{p}^2(R_i)+\Delta_{v_{
             i}}^{2}}} \exp\biggl [-\frac{1}{2}\biggl (\frac{(v_{\rm
           i}
         -\bar{v})^{2}}{\sigma_{p}^2(R_i)+\Delta_{v_{i}}^{2}}\biggr
       )\biggr ],

   where the dispersion of velocities at radius :math:`R_i` of the :math:`i`-th star comes from both the intrinsic dispersion :math:`\sigma_{p}(R_i)` and the measurement uncertainty :math:`\Delta_{v_{i}}`


**Analysis with or without membership probabilities**

   Kinematic samples are often contaminated by interlopers from the Milky Way (MW) foreground stars. Different methods can be used in order to remove those contaminants, based e.g. on sigma-clipping, virial theorem,
   or expectation maximisation (EM) algorithms. The latter allows in particular the estimation of membership probabilities for each star of the object. These probabilities can be used as weights when building the
   velocity dispersion profile :math:`\sigma_{\rm obs}(R)` in the binned case, or used directly in the unbinned likelihood, where the above equation becomes

   .. math::

      \mathcal{L}^{\rm unbin}_{W}= \prod_{i=1}^{N_{\rm stars}}
      \left(\frac{(2\pi)^{-1/2}}{\sqrt{\sigma_{p}^2(R_i)+\Delta_{v_{i}}^{2}}} \exp\biggl [-\frac{1}{2}\biggl
       (\frac{(v_{\rm i} -\bar{v})^{2}}{\sigma_{p}^2(R_i)+\Delta_{v_{i}}^{2}} \biggr )\biggr ] \right)^{P_{i}},

   with :math:`P_{i}` the membership probability of the :math:`i`-th star.


