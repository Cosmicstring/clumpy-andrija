.. _rst_physics_1Dtofux:


*J*-factors to fluxes
-------------------------

.. note :: This section treats the conversion to fluxes/intensities of 1D runs. See :numref:`rst_doc_module_f` for similarly converting 2D *J*-factor maps to flux per pixel/intensity maps.


The flux from a single source or from the Galactic halo within a solid angle :math:`\Delta\Omega` can be computed in two manners:

1. Computing the *differential energy spectrum* for a fixed *J*-factor or solid angle :math:`\Delta\Omega`:

   This is done by :program:`CLUMPY`'s :option:`-z` module (see Section :numref:`rst_doc_modules`):

   .. code-block:: console

       $ clumpy -z -D; clumpy -z -i clumpy_params_z.txt

   This first generates a default parameter file, and then the :math:`\gamma`-ray flux energy spectrum for a source at redshift :math:`z=0` and *J*-factor :math:`J = 10^{20}\,{\rm GeV}^2{\rm ~cm}^{-5}`. If :program:`ROOT` is installed, the following plot is shown:

   .. _fig_zD:

   .. figure:: DocImages/zD-1.png
      :align: center
      :figclass: align-center
      :scale: 65%

      :program:`CLUMPY`'s :option:`-z` module to compute flux energy spectra for a given *J*-factor.


2. Computing a flux (differential or integrated in energy) as a function of a **varying** *J*-**factor or solid angle** :math:`\Delta\Omega`:

   This is done by :program:`CLUMPY`'s :option:`-g2` or :option:`-h2` modules (see :numref:`rst_doc_modules`), depending of considering Galactic DM or a (list of) user-defined halo(s). Here, the user may choose to plot the differential flux or the integrated flux,

   .. math :: \Phi_{\gamma,\nu}(E_{\rm min},E_{\rm max};\,\psi,\theta,\Delta\Omega) = \int\limits_{E_{\rm min}}^{E_{\rm max}}\frac{{\rm d} \Phi_{\gamma,\nu}}{{\rm d} E}(E,\psi,\theta,\Delta\Omega)\, {\rm d}E\,.


   .. note:: The option :option:`gSIM_IS_WRITE_FLUXMAPS = True` must be enabled here.

   For the example of the flux from Galactic DM within a solid angle of :math:`\Delta\Omega` of an instrument pointed toward the Galactic anti-center :math:`(\psi_0, \theta_0) = (\pi, 0)`:

   .. code-block:: console

       $ clumpy -g2 -D; clumpy -g2 -i clumpy_params_g2.txt

  If :program:`ROOT` is installed, the following plot of the output data is shown:

   .. _fig_g2D:

   .. figure:: DocImages/g2D-2.png
      :align: center
      :figclass: align-center
      :scale: 65%

      :program:`CLUMPY`'s :option:`-g2` with default parameters to compute the flux (here: integrated in energy) from Galactic DM as a function of the integration radius :math:`\alpha_{\rm int}` of the search cone :math:`\Delta\Omega`.

   Using

   .. code-block:: console

       $ clumpy -h2 -D; clumpy -h2 -i clumpy_params_h2.txt

   produces analogous plots for the remote haloes defined in the :file:`$CLUMPY/data/list_generic.txt` example file (see :numref:`rst_doc_modules` and the :ref:`rst_quick_start`):

   .. _fig_h2D-flux:

   .. figure:: DocImages/h2D-flux.png
      :align: center
      :figclass: align-center
      :scale: 65%

      :program:`CLUMPY`'s :option:`-h2` module with default parameters to compute the flux (here: integrated in energy) from user-defined haloes as a function of the integration radius :math:`\alpha_{\rm int}`.


In addition to these two possibilies, the :option:`-g3`/:option:`-g4` or :option:`-h3` modules can be used analogous to :option:`-g2` or :option:`-h2` to compute the *intensities* from Galactic DM or individual halo objects. For example,

   .. code-block:: console

       $ clumpy -h3 -D; clumpy -h3 -i clumpy_params_h3.txt

   compares the intensity profiles of the Abell Cluster and a mock dSphG halo:

   .. _fig_h3D-intensities:

   .. figure:: DocImages/h3D-intensities.png
      :align: center
      :figclass: align-center
      :scale: 65%

      :program:`CLUMPY`'s :option:`-h3` module with default parameters to compute :math:`{\rm d}\Phi/{\rm d}\Omega` (here:  integrated in energy) as a function of the distance :math:`\theta` from the halo centre.



