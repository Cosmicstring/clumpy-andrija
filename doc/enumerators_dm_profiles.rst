.. _rst_enumerators_dm_profiles:

.. role::  raw-html(raw)
    :format: html

Profiles :math:`\rho_{\rm DM}` and :math:`{\rm d}{\cal P}_V/{\rm d}V`
------------------------------------------------------------------------------------


Possible keyword choices for :option:`gXXX_FLAG_PROFILE` variables:

.. csv-table:: 
   :file: tables/gENUM_PROFILE.csv_table
   :widths: 10, 30
   :align: left

:raw-html:`<br>&nbsp;<br>`

.. note::
   All above profiles are spherically symmetric as a function of the radial coordinate :math:`r`. :raw-html:`<br>`
   Additionally, they depend on a characteristic scale length, a density normalisation [#f1]_, and up to three  dimensionless shape parameters. In the above notation,
   
   .. math:: \rho = \rho\left(\,r\,|\,\rm scale\, length,\,density\, normalisation;\,shape\, parameters\right).

   - A triaxial distortion can be applied to all profiles as explained in :numref:`rst_trixial`. :raw-html:`<br><br>`
   
   - For the Milky Way halo, the scale length and density normalisation are set via the Sun's distance from the Galactic centre and the local DM density, 
   
     .. math:: \tt{gMW\_RSOL} \quad \sf{and} \quad \tt{gMW\_RHOSOL}.
     
     For :keyword:`kNODES` numerical profile, :option:`gMW_RHOSOL` can be set to :keyword:`-1` and the absolute values from the input node points are used to describe the halo profile and the local DM density. Note that  :option:`gMW_TOT_RSCALE` is meaningless in this case. :raw-html:`<br><br>`
   
   - User-defined extragalactic or dSph haloes are defined in a definition file (as, e.g., the template file ``$CLUMPY/data/list_generic.txt``), where the scale length and density normalisation are specified as :math:`\tt rs` and :math:`\tt rhos`, respectively. :raw-html:`<br><br>`
   
   - The shape parameters are controlled via the
   
     .. math:: \tt gXXX\_SHAPE\_PARAMS\_0,\, gXXX\_SHAPE\_PARAMS\_1,\, gXXX\_SHAPE\_PARAMS\_2
   
     input parameters (when parsed via the command line or the global parameter file) or as ``#1``, ``#2``, ``#3`` (in a halo definition file) **according to the order as above**. :raw-html:`<br><br>`

   - The density normalisation and scale radius can also be obtained by providing the total halo mass :math:`M_{\Delta}` of a halo with the corresponding overdensity definition :math:`{\Delta}` and a concentration-mass relation :math:`c_\Delta(M_\Delta)`. In CLUMPY, the density profiles of all subhaloes are defined this way. :raw-html:`<br><br>`
   
   - Using a profile as :math:`{\rm d}{\cal P}_V/{\rm d}V` function (see :numref:`rst_prob_distributions`) does not need a density normalisation to be provided, as the probability distribution is internally normalised to one. The scale radii of :math:`{\rm d}{\cal P}_V/{\rm d}V` profiles are defined by the user relative to the host halo scale radius, via the
   
     .. math:: \tt gXXX\_SUBS\_DPDV\_RSCALE\_TO\_RS\_HOST

     input variables.

.. seealso::
   :numref:`rst_physics_profiles`, :ref:`rst_physics_profiles`.

.. [#f1] Despite the profiles only used to describe :math:`{\rm d}{\cal P}_V/{\rm d}V`, where the normalisation is automatically chosen such that :math:`\int_{0}^{R_{vir}} \frac{{\rm d}{\cal P}_V(r)}{{\rm d}V}\, {\rm d}^{3}\vec{r} = 1`.