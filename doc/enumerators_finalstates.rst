.. _rst_enumerators_finalstates:

.. role::  raw-html(raw)
    :format: html

Standard-model final states
--------------------------------------------------------------

Possible keyword choices for the :option:`gSIM_FLUX_FLAG_FINALSTATE` variable:

.. csv-table::
   :file: tables/gENUM_FINALSTATE.csv_table
   :widths: 10, 50
   :align: left

:raw-html:`<br>&nbsp;<br>`

.. seealso::
   :numref:`rst_doc_finalstates`.

