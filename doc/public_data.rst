.. _rst_data:

.. sectionauthor:: XX

.. role::  raw-html(raw)
    :format: html


Publications and data files
=========================================

We only report below publications co-authored by a :program:`CLUMPY` developer. 

Dark haloes
-----------

1. | *DM substructure modelling and sensitivity of the CTA to Galactic dark halos.*
   | :raw-html:`<A href="http://adsabs.harvard.edu/abs/2016JCAP...09..047H" target="_blank">H&uuml;tten, Combet, Maier, and Maurin, JCAP 09, 047 (2016)</A>`

2. | *γ-ray and ν searches for dark matter subhalos in the Milky Way with a baryonic potential.*
   | :raw-html:`<A href="http://adsabs.harvard.edu/abs/2019arXiv190410935H" target="_blank">H&uuml;tten, Stref, Combet, Lavalle, and Maurin, Galaxies 7(2), 60 (2019)</A>`
   |     Subhalo skymaps and catalogs available upon request to `clumpy@lpsc.in2p3.fr <clumpy@lpsc.in2p3.fr>`__.


Dwarf spheroidal galaxies
--------------------------

1. | *DM in the classical dSphs: a robust constraint on the J-factor for γ-ray flux calculations.*
   | :raw-html:`<a href="http://cdsads.u-strasbg.fr/abs/2011ApJ...733L..46W" target="_blank">Walker et al., ApJL 733, 46 (2011)</a>`
   |

2. | *DM profiles and annihilation in dSphs: prospectives for γ-ray observatories.*
   | :raw-html:`<A href="http://cdsads.u-strasbg.fr/abs/2011MNRAS.418.1526C" target="_blank">Charbonnier et al., MNRAS 418, 1526 (2011)</A>`
   |     :download:`J-factor 68% and 95% confidence levels for various integration angles <DocData/classical_dsphs_CLs.tar.gz>`
   |

3. | *DM annihilation and decay in dSphs: The classical and ultrafaint dSphs.*
   | :raw-html:`<A href="http://cdsads.u-strasbg.fr/abs/2015MNRAS.453..849B" target="_blank">Bonnivard et al., MNRAS 453, 1 (2015)</A>`
   |     :download:`J- and D-factor 68% and 95% confidence levels for various integration angles<DocData/J_and_D_dsphs_CLs.tar.gz>`
   |

4. | *DM annihilation and decay profiles for the Reticulum II dSph galaxy.*
   | :raw-html:`<A href="http://cdsads.u-strasbg.fr/abs/2015ApJ...808L..36B" target="_blank">Bonnivard et al., ApJL 808, 2 (2015)</A>`
   |    :download:`J-factor 68% and 95% confidence levels for various integration angles<DocData/ret2.Jalphaint_cls.output>`
   |    :download:`D-factor 68% and 95% confidence levels for various integration angles<DocData/ret2.Dalphaint_cls.output>`
   |    :download:`DM profile best-fit parameters for Reticulum II (not in the publication)<DocData/rho_bestfit_ret2.tar.gz>`
   |

5. | *Spherical Jeans analysis for DM indirect detection in dSph galaxies - impact of physical parameters and triaxiality.*
   | :raw-html:`<A href="http://cdsads.u-strasbg.fr/abs/2015MNRAS.446.3002B" target="_blank">Bonnivard, Combet, Maurin, and Walker, MNRAS 446, 3002 (2015)</A>`
   |    :download:`DM profile best-fit parameters for all dSphs analyzed (not in the publication)<DocData/rho_bestfit_dsphs.tar.gz>`
   |

6. | *Contamination of stellar-kinematic samples and uncertainty about DM annihilation profiles in ultrafaint dSphs: the example of Segue I.*
   | :raw-html:`<A href="http://adsabs.harvard.edu/abs/2016MNRAS.462..223B" target="_blank">Bonnivard, Maurin, and Walker, MNRAS 462, 223 (2015)</A>`
   |

7. | *Magellan/M2FS Spectroscopy of Tucana 2 and Grus 1.*
   | :raw-html:`<A href="http://cdsads.u-strasbg.fr/abs/2016ApJ...819...53W" target="_blank">   Walker et al., ApJ 819, 53 (2016)</A>`


Galaxy clusters
---------------

1. | *Decaying DM: stacking analysis of galaxy clusters to improve on current limits.*
   | :raw-html:`<A href="http://cdsads.u-strasbg.fr/abs/2012PhRvD..85f3517C" target="_blank">Combet et al., PhRvD 85, 063517 (2012)</A>`
   |    :download:`Decay (D) astrophysical factor for all objects of the MCXC catalog <DocData/D_MCXC.txt>`
   |

2. | *γ-rays from annihilating DM in galaxy clusters: stacking versus single source analysis.*
   | :raw-html:`<A href="http://cdsads.u-strasbg.fr/abs/2012MNRAS.425..477N" target="_blank">Nezri et al., MNRAS 425, 477 (2012)</A>`
   |    :download:`Annihilation astrophysical factor (J) for all objects of the MCXC catalog<DocData/J_MCXC.txt>`
   |

3. | *Disentangling cosmic-ray and dark-matter induced γ-rays in galaxy clusters.*
   | :raw-html:`<a href="http://cdsads.u-strasbg.fr/abs/2012A%26A...547A..16M" target="_blank">Maurin et al., A&A 547, 16 (2012)</a>`


Extragalactic
-------------

1. | *Extragalactic diffuse γ-rays from DM annihilation: revised prediction and full modelling uncertainties.*
   | :raw-html:`<A href="http://adsabs.harvard.edu/abs/2018JCAP...02..005H" target="_blank">H&uuml;tten, Combet, and Maurin, JCAP 02, 005 (2018)</A>`
   |    :download:`Parameter files to recompute all annihilation spectra considered in the article<DocData/paramfiles_extragal.tar.gz>`