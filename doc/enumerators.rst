.. _rst_enumerators:

Keywords for ingredient selection
=======================================

The following lists contain all choices for non-numeric keyword values of various input parameters:

.. toctree::
   :maxdepth: 1

   enumerators_delta
   enumerators_dm_profiles
   enumerators_cdelta
   enumerators_massfunction
   enumerators_tauEBL
   enumerators_spectra
   enumerators_growthfactor
   enumerators_windowfunc
   enumerators_nuflavours
   enumerators_finalstates


For the Jeans analysis, the following keywords can be chosen in the syntax of the :option:`gLIST_HALOES_JEANS` file:

.. toctree::
   :maxdepth: 1

   enumerators_anisotropyprofiles
   enumerators_lightprofiles