# -*- coding: utf-8 -*-
#
# CLUMPY documentation build configuration file, based on
# healpy documentation scheme.
#
# This file is execfile()d with the current directory set to its containing dir.
#
# The contents of this file are pickled, so don't put values in the namespace
# that aren't pickleable (module imports are okay, they're removed automatically).
#
# All configuration values have a default value; values that are commented out
# serve to show the default value.

import sys, os, re
import datetime


sys.path.insert(0, os.path.abspath('_ext'))

# If your extensions are in another directory, add it here. If the directory
# is relative to the documentation root, use os.path.abspath to make it
# absolute, like shown here.
sys.path.append(os.path.abspath('.'))

# General configuration
# ---------------------

# Add any Sphinx extension module names here, as strings. They can be extensions
# coming with Sphinx (named 'sphinx.ext.*') or your custom ones.

extensions = ['sphinx.ext.autosummary', 
              'sphinx.ext.autodoc', 
              'sphinx.ext.mathjax',
              'easydev.copybutton',
              ]
import easydev
jscopybutton_path = easydev.copybutton.get_copybutton_path()
import shutil
shutil.copy(jscopybutton_path, '_static')

suppress_warnings = ['ref.keyword', 'ref.option']

# Tables, figures get a number:
numfig = True

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates/']

# The suffix of source filenames.
source_suffix = '.rst'

# The master toctree document.
master_doc = 'index'

# General substitutions.
project = u'CLUMPY'
copyright = u'2018, M. Hütten, C. Combet, D. Maurin'
author = u'M. Hütten, C. Combet, D. Maurin <clumpy@lpsc.in2p3.fr>'

# The default replacements for |version| and |release|, also used in various
# other places throughout the built documents.
#
# The short X.Y version.

release = re.sub('^v', '', os.popen('git describe').read().strip())

major = release.split('-')[0]
revision = release[-7:]

# The short X.Y version.
version = "version " + major

# The full version, including alpha/beta/rc tags.
release = version

# There are two options for replacing |today|: either, you set today to some
# non-false value, then it is used:
#today = ''
# Else, today_fmt is used as the format for a strftime call.
today_fmt = '%B %d, %Y'

# List of documents that shouldn't be included in the build.
#unused_docs = []

# List of directories, relative to source directories, that shouldn't be searched
# for source files.
exclude_patterns = ['_build/*', '_templates/*', 'ext/*.rst']

# The reST default role (used for this markup: `text`) to use for all documents.
#default_role = None

# If true, '()' will be appended to :func: etc. cross-reference text.
#add_function_parentheses = True

# If true, the current module name will be prepended to all description
# unit titles (such as .. function::).
# add_module_names = True

# If true, sectionauthor and moduleauthor directives will be shown in the
# output. They are ignored by default.
#show_authors = False

# The name of the Pygments (syntax highlighting) style to use.
#pygments_style = 'sphinx'


# Options for HTML output
# -----------------------

import sphinx_rtd_theme
html_theme = 'sphinx_rtd_theme'

# The name for this set of Sphinx documents.  If None, it defaults to
# "<project> v<release> documentation".
html_title = 'CLUMPY documentation'

# A shorter title for the navigation bar.  Default is the same as html_title.
#html_short_title = None

# The name of an image file (relative to this directory) to place at the top
# of the sidebar.
html_logo = 'DocImages/clumpy_logo.png'

# The name of an image file (within the static path) to use as favicon of the
# docs.  This file should be a Windows icon file (.ico) being 16x16 or 32x32
# pixels large.
#html_favicon = None

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static/', 'DocImages/', 'DocData/']

html_extra_path  = ['doxygen/']


# The style sheet to use for HTML and HTML Help pages. A file of that name
# must exist either in Sphinx' static/ path, or in one of the custom paths
# given in html_static_path.
#html_style = 'default.css'

# If not '', a 'Last updated on:' timestamp is inserted at every page bottom,
# using the given strftime format.
html_last_updated_fmt = '%b %d, %Y'

now = datetime.datetime.now()
date = now.strftime(html_last_updated_fmt)

html_context = {
"display_gitlab": True, # Add 'Edit on Github' link instead of 'View page source'
"last_updated":  date + " (" + revision + ")",
"commit": False,
"gitlab_user": 'clumpy',
"gitlab_repo": 'clumpy',
"gitlab_version": 'doc',
"conf_py_path": 'doc',
}

# If true, SmartyPants will be used to convert quotes and dashes to
# typographically correct entities.
#html_use_smartypants = True

# Custom sidebar templates, maps document names to template names.
html_sidebars = {
#    'index': 'indexsidebar.html'
}

# Additional templates that should be rendered to pages, maps page names to
# template names.
html_additional_pages = {
#    'index': 'indexcontent.html',
}

html_theme_options = {
    'display_version': True}

# If false, no module index is generated.
#html_use_modindex = True

# If false, no index is generated.
#html_use_index = True

# If true, the index is split into individual pages for each letter.
#html_split_index = False

# If true, the reST sources are included in the HTML build as _sources/<name>.
#html_copy_source = True

# If true, an OpenSearch description file will be output, and all pages will
# contain a <link> tag referring to it.  The value of this option must be the
# base URL from which the finished HTML is served.
#html_use_opensearch = ''

# If nonempty, this is the file name suffix for HTML files (e.g. ".xhtml").
#html_file_suffix = ''

# Output file base name for HTML help builder.
htmlhelp_basename = 'clumpydoc'


# Options for LaTeX output
# ------------------------

# The paper size ('letter' or 'a4').
#latex_paper_size = 'letter'

# The font size ('10pt', '11pt' or '12pt').
#latex_font_size = '10pt'

# Grouping the document tree into LaTeX files. List of tuples
# (source start file, target name, title, author, document class [howto/manual]).
latex_documents = [
  (master_doc, 'clumpy_doc.tex', u'CLUMPY Documentation',
   author, 'manual'),
]

# The name of an image file (relative to this directory) to place at the top of
# the title page.
latex_logo = 'DocImages/clumpy_snapshots.png'

# For "manual" documents, if this is true, then toplevel headings are parts,
# not chapters.
latex_use_parts = False

# Additional stuff for the LaTeX preamble.
latex_preamble = '\setcounter{tocdepth}{3}'

# Documents to append as an appendix to all manuals.
#latex_appendices = []

# If false, no module index is generated.
#latex_use_modindex = True

import glob
autosummary_generate = glob.glob('*.rst')

def setup(app):
   app.add_stylesheet("theme_overrides.css")


