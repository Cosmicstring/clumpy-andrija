.. _rst_enumerators_anisotropyprofiles:

.. role::  raw-html(raw)
    :format: html

Anisotropy profiles :math:`\beta_{\rm ani}(r)`
--------------------------------------------------------------

Possible keyword choices for the anisotropy profile to be set in the file :option:`gLIST_HALOES_JEANS` (no named variable):

:raw-html:`<center><small>`

.. csv-table:: 
   :file: tables/gENUM_ANISOTROPYPROFILE.csv_table
   :widths: 10, 30,30,30,30

:raw-html:`</small></center><br>&nbsp;<br>`

.. seealso::
   :numref:`rst_physics_jeans`, :ref:`rst_physics_jeans`.
