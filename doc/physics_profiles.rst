.. _rst_physics_profiles:

.. sectionauthor:: David Maurin <dmaurin@lpsc.in2p3.fr>

.. role::  raw-html(raw)
    :format: html

Halo profiles (profiles.h)
--------------------------

Formal description
~~~~~~~~~~~~~~~~~~

A DM profile is characterised by a normalisation :math:`\rho_{\rm s}`, a scale radius :math:`r_{\rm s}`, and several shape parameters :math:`\vec{\alpha}`. For a spherically symmetric DM halo, we have formally

   .. math::

      \rho(r) = f(\rho_{\rm s},r_{\rm s}, \vec{\alpha})\,.


The *physical* size of the DM halo is usually taken to be :math:`R_{\rm \Delta}`, the radius at overdensity :math:`\Delta`. All profiles and their corresponding input parameter keyword names are gathered in :numref:`rst_enumerators_dm_profiles`. Their calculation is implemented in the library `profiles.h <doxygen/profiles_8h.html>`_.


Saturation radius :math:`r_{\rm sat}`
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Some of these DM profiles are steep enough in the inner regions to lead to a singularity of the :math:`\gamma`-ray luminosity at the centre of the halo. However, a cut-off radius :math:`r_{\rm sat}` naturally appears, within  which the DM density saturates due to the balance between the annihilation rate and the gravitational infalling rate of DM particles. The saturation density reads :raw-html:`<a href="http://adsabs.harvard.edu/abs/1992PhLB..294..221B" target="_blank">(Berezinsky et al., 1992)</a>`:

   .. math::
      \rho_{\rm sat}= 3.10^{18} \left(\frac{m_\chi}{100~\rm GeV}\right) \times \left( \frac{10^{-26} {\rm cm}^3~{\rm s}^{-1}} {\langle \sigma v\rangle}\right) M_\odot~{\rm kpc}^{-3}.

The saturation radius is defined to be the radius for which :math:`\rho(r_{\rm sat})=\rho_{\rm sat}`, so that :math:`\rho(r<r_{\rm sat})=\rho_{\rm sat}`. This is a very crude description, but this cut-off only arises for the steepest profiles which are generally disfavoured. For the various family of profiles we use in :program:`CLUMPY`, we have:

   - :keyword:`kEINASTO`/:keyword:`kEINASTO_N` family: for any model in Einasto, the profile is not cuspy, so :math:`\rho_{\rm sat}` is assumed to never be reached.
   - :keyword:`kZHAO` family: :math:`r_{\rm sat} = r_s \times \left(\frac{\rho_{\rm s}}{\rho_{\rm sat}}\right)^{1/\gamma}`.
   - :keyword:`kDPDV_GAO04`: if :math:`\beta-3<0`: :math:`r_{\rm sat} = r_{200} \times \left\{\left(\frac{\rho_{\rm sat}}{\rho_{200}}\right) \frac{\left[\beta-\alpha ac/(1+ac)\right]}{\beta(1+ac)}\right\}^{1/(\beta-3)}\;`.

.. note::
  In :program:`CLUMPY`, the user cannot select :math:`r_{\rm sat}` for a particular halo, but must globally set a physically motivated value for :math:`\rho_{\rm sat}`, :math:`[\rho_{\rm sat}]={\rm M}_\odot\,{\rm kpc}^{-3}`, *for all (sub)halos* involved in the simulation. This is done by the :option:`gDM_RHOSAT` input parameter.

.. _rst_r2:

Radius :math:`r_{-2}`
~~~~~~~~~~~~~~~~~~~~~

The concentration of a DM halo, defined in :numref:`rst_physics_clumps_generic`, depends on :math:`r_{-2}`, the radius for which the slope is :math:`-2`:

.. math::
     \left. \frac{{\rm d}~[r^2 \rho(r)~]}{{\rm d}r}\right|_{r=r_{-2}} = 0\quad \Leftrightarrow\quad \left.\frac{{\rm d}\log \rho}{{\rm d}\log r}\right|_{r=r_{-2}} = -2.

For any specific profile, it can be related to the scale radius (:math:`r_{\rm s},\,r_{0},\,r_{\rm e},...`) defining a given halo's dimension, as long as the outer slope of the profile is steeper than :math:`-2`. Values for :math:`r_{-2}` are gathered in :numref:`rst_enumerators_dm_profiles`. To highlight the position of :math:`r_{-2}` in various profiles, :numref:`fig_r2rho_allprofiles` shows the quantity :math:`r^2\rho(r)` for Milky-Way like halos.

.. _fig_r2rho_allprofiles:

.. figure:: DocImages/r2rho_allprofiles.png
   :align: center
   :figclass: align-center
   :scale: 80%

   Radius :math:`r_{-2}` for various DM profiles (see :numref:`rst_enumerators_dm_profiles`) at the maximum of the curves.



.. _rst_trixial:

Triaxial profiles
~~~~~~~~~~~~~~~~~

1. **Definition:**

   Triaxial profiles are described for instance in :raw-html:`<a href=http://adsabs.harvard.edu/abs/2002ApJ...574..538J target="_blank"> Jing & Suto, ApJ 574, 538 (2002)</a>.` We refer the reader to the :raw-html:`<a href=http://en.wikipedia.org/wiki/Ellipsoid target="_blank">Wikipedia</A>` page for a general description of an ellipsoid: the points :math:`(a,0,0)`, :math:`(0,b,0)` and :math:`(0,0,c)` lie on the surface and the line segments from the origin to these points are called the semi-principal axes of length :math:`a,b,c`. They correspond to the semi-major axis and semi-minor axis of the appropriate ellipses. :raw-html:`<br><br>`


2. **Convention in CLUMPY:**

   In :program:`CLUMPY`, we allow for a general triaxial model :math:`(a\neq b\neq c)`, where the major axis is :math:`a` the intermediate axis :math:`b`, and the minor axis is :math:`c`. Cosmological simulations have shown that these axes can vary as a function of the iso-density radius :math:`R_{\rm iso}` :raw-html:`<a href=http://adsabs.harvard.edu/abs/2002ApJ...574..538J target="_blank"> (Jing & Suto, ApJ 574, 538 2002)</a>,` but it is not considered here. In any case, the position :math:`(X,Y,Z)` in the coordinate framework attached to a given DM halo centre (see :numref:`fig_geometry`) corresponds to the iso-density radius

   .. math::
        R_{\rm iso} =\sqrt{\frac{X^2}{a^2} + \frac{Y^2}{b^2} + \frac{Z^2}{c^2}}.

   The density is given by :math:`\rho(R)`, where :math:`\rho` is any spherical profile discussed above. :raw-html:`<br><br>`


3. **Initialisation parameters:**

   Triaxial parameters consist of 7 parameters (three axes for the shape, three :ref:`rst_euler_angles`, and one boolean to decide whether to take into account or not the triaxiality). Depending of the type of halo considered, triaxiality is implemented in different fashion in :program:`CLUMPY`:

   - *Galactic halo:*

     Triaxial parameters for the Galactic halo :option:`gMW_XXX` are set directly in the parameter file or over the command line as given in :numref:`tab_set_triax_gal`:

     :raw-html:`<center>`

     .. _tab_set_triax_gal:

     .. table:: Setting triaxiality for the Milky Way halo

           ======================================= ========================================
           **Quantities (triaxial profile)**       **Galactic halo: parameter names**
           --------------------------------------- ----------------------------------------
           Is triaxiality enabled? (0 or 1)        :option:`gGAL_TRIAXIAL_IS`
           :math:`a\;\;[-]`                        :option:`gGAL_TRIAXIAL_AXES_0`
           :math:`b\;\;[-]`                        :option:`gGAL_TRIAXIAL_AXES_1`
           :math:`c\;\;[-]`                        :option:`gGAL_TRIAXIAL_AXES_2`
           :math:`\alpha_{\rm Euler}\;\;[\rm deg]` :option:`gGAL_TRIAXIAL_ROTANGLES_0`
           :math:`\beta_{\rm Euler}\;\;[\rm deg]`  :option:`gGAL_TRIAXIAL_ROTANGLES_1`
           :math:`\gamma_{\rm Euler}\;\;[\rm deg]` :option:`gGAL_TRIAXIAL_ROTANGLES_2`
           ======================================= ========================================

   :raw-html:`</center>`

   - *User-defined list of halos:*

     Triaxial parameters are set directly in the file of user-defined haloes  as extra-columns in the file (regardless of the type of the DM halo, be it a galaxy, a dwarf spheroidal galaxy, or a cluster of galaxy), see :ref:`rst_halofile_format` for the correct format. :raw-html:`<br><br>`

   - *Substructures:*

     Triaxiality is not enabled/implemented in :program:`CLUMPY`, since it would require a statitical distribution of axis ratios and orientations. First, the orientation is not important if the integration angle encompasses the halo volume (and this is the case for all subhaloes but a tiny fraction). Second, the exact distribution of axis ratios is not known, but the impact in the calculation is certainly negligible compared to other uncertainties (concentration, profile, etc.). In the end, it is not worth including this option in the calculation.

.. seealso:: :ref:`rst_euler_angles`


.. _rst_doc_dm_families:


Family of profiles
~~~~~~~~~~~~~~~~~~


Zhao (:keyword:`kZHAO`)
+++++++++++++++++++++++

Many DM profiles used in the literature can be written in terms of a three parameter :math:`(\alpha,\beta,\gamma)` family :raw-html:`(<a href=http://cdsads.u-strasbg.fr/abs/1990ApJ...356..359H target="_blank">Hernquist 1990</a> and <a href=http://cdsads.u-strasbg.fr/abs/1996MNRAS.278..488Z target="_blank"> Zhao 1996</a>):`

  .. math::
        \rho\left(r\,|\,r_{\rm s},\rho_{\rm s};\alpha,\beta,\gamma\right)=\frac{2^{\frac{\beta-\gamma}{\alpha}}\,\times\,\rho_{\rm s}}{\left(\frac{r}{r_{\rm s}}\right)^\gamma \times \left[1+\left(\frac{r}{r_{\rm s}}\right)^\alpha\right]^{\frac{\beta-\gamma}{\alpha}}}\;.

For this profile, we have :math:`r_{-2} = r_{\rm s} \times \left( \frac{2-\gamma}{\beta-2} \right)^{\frac{1}{\alpha}}`, with

  - :math:`r_{\rm s}`: scale radius, :math:`[r_{\rm s}]={\rm kpc}`,
  - :math:`\rho_{\rm s}`: density normalisation :math:`\rho(r_{\rm s})`, :math:`[\rho_{\rm s}]={\rm M}_\odot\,{\rm kpc}^{-3}`,
  - :math:`\alpha`: logarithmic transition slope (controls the sharpness of the transition between the inner and outer slopes),
  - :math:`\beta`: outer logarithmic slope,
  - :math:`\gamma`: inner logarithmic slope.

Widely-used *special cases* of the Hernquist-Zhao profile are:

  - **Modified Isothermal** :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/1991MNRAS.249..523B target="_blank">(Begeman, Broeils, Sanders, 1991)</a>` with :math:`(\alpha,\beta,\gamma)=(2,2,0)`;
  - **NFW** :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/1997ApJ...490..493N target="_blank">(Navarro, Frenk & White, 1997)</a>` with :math:`(\alpha,\beta,\gamma)=(1,3,1)`;
  - **Moore** :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/1998ApJ...499L...5M target="_blank">(Moore et al., 1998)</a>` with :math:`(\alpha,\beta,\gamma)=(1.5,3,1.5)`. :raw-html:`<br>`


Einasto profiles
++++++++++++++++

In the last decade, it has been realised that the inner slope of the DM halo should decrease as :math:`r` decreases, which has given rise to several logarithmic inner-slope parametrisations, generally based on the Einasto profile which follows

  .. math:: \rho(r)\propto \exp\left(-Ar^\alpha\right)\quad \Leftrightarrow \quad \frac{{\rm d}\log \rho}{{\rm d}\log r} = -\alpha A \,r^{\alpha},
    :label: eq_einasto

and was first used by Einasto to describe the mass and light distributions in galaxies. Note that it is formally identical to Sersic's law which is traditionally applied to projected (2D) density profiles rather that 3D density profiles. In the literature, two main expressions of the Einasto density profile of DM haloes have been formulated (see also :raw-html:`<a href=http://arxiv.org/abs/1202.5242 target="_blank">Retana-Montenegro et al. (2012)</a>` for a nice and comprehensive discussion of the properties of Einasto profiles):

  1. **Standard** Einasto description (:keyword:`kEINASTO`):

    The standard Einasto is used for instance in :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/2004MNRAS.349.1039N target="_blank">Navarro et al. (2004)</a> and <a href=http://adsabs.harvard.edu/abs/2008MNRAS.391.1685S target="_blank">Springel et al. (2008)</a>,` where :math:`A = \frac{2}{\alpha}\, r_{-2}^{\;-\alpha}`

       .. math::

            \Rightarrow \quad \rho^{\rm EINASTO}\left(r\,|\,r_{-2},\rho_{-2};\alpha\right)=\rho_{-2}\, \exp\left\{-\frac{2}{\alpha}\left[\left(\frac{r}{r_{-2}}\right)^\alpha -1\right]\right\}\;,

    and both teams find that the shape parameter :math:`\alpha=0.17` as a good description of the density profile of DM halos. :raw-html:`<a href=http://adsabs.harvard.edu/abs/2008MNRAS.391.1685S target="_blank"> Springel et al. (2008)</a>` also find that :math:`\alpha=0.68` describes well the spatial distribution of subhaloes :math:`{\rm d}{\cal P}/{\rm d}V` in the Milky-Way.

    For the standard Einasto profile, we have :math:`\rho(r_{-2}) = \rho_{-2}`, with

    - :math:`r_{-2}`: radius, :math:`[r_{-2}] ={\rm kpc}`, for which the slope is :math:`-2`,
    - :math:`\rho_{-2}`: normalisation, :math:`[\rho_{-2}] = {\rm M}_\odot\,{\rm kpc}^{-3}`.
    - :math:`\alpha:` parameter controlling the sharpness of the rolling logarithmic slope. :raw-html:`<br>`

  2. **Einasto 's** :math:`r^{1/n}` **profile**  (:keyword:`kEINASTO_N`):

    :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/2006AJ....132.2685M target="_blank"> Merritt et al. (2006)</a>` implemented a so-called Einasto's ":math:`r^{1/n}` model",

       .. math::
            \alpha = \frac{1}{n},\quad A&=d_n\, r_{\rm e }^{\;-1/n}\\[0.3cm]
               \Rightarrow \quad   \rho^{\rm EINASTO_N}\left(r\,|\,r_{\rm e},\rho_{\rm e};n\right)& = \rho_{\rm e} \, \exp\left\{-d_n \times \left[ \left(\frac{r}{r_{\rm e}}\right)^{\frac{1}{n}} - 1 \right]\right\}\,,

    with :math:`d_n` being given by the implicit equation

    .. math::

           \Gamma(3n) = 2\,\gamma(3n,\,d_n)\,,

    where :math:`\Gamma(a)` is the complete gamma function and :math:`\gamma(a,x)` the lower incomplete gamma function. For :math:`n\gtrsim 0.5`, :math:`d_n` can be approximately written explicitly as a function of :math:`n` :raw-html:`(<a href=http://cdsads.u-strasbg.fr/abs/2006AJ....132.2685M target="_blank">Merritt et al., 2006</a>):`

       .. math:: d_n \approx 3n-\frac{1}{3}+\frac{0.0079}{n}\,.


    For :math:`n` an integer, which is mandatory in :program:`CLUMPY`, :math:`M_{\rm tot} \sim 2\pi \,n\, r_{\rm e}^3\, \rho_{\rm e}\, e^{d_n}\, d_n^{-3n} (3n-1)!` :raw-html:`(<a href=http://cdsads.u-strasbg.fr/abs/2006AJ....132.2685M target="_blank">Merritt et al. 2006</a>).` Like for the standard Einasto profile, the profile :keyword:`kEINASTO_N` provides good fits to N-body simulations with :math:`n=6` (:math:`{\,n}^{-1}={1}/{6}\approx 0.17`).

    For this profile, we have :math:`r_{-2} = r_{\rm e} \times \left( \frac{2 n}{d_n}\right)^n` :raw-html:`<A href=http://cdsads.u-strasbg.fr/abs/2006AJ....132.2701G target="_blank">(Graham et al. 2006)</A>,` with which the profiles :keyword:`kEINASTO`  and :keyword:`kEINASTO_N` can be translated one into the other, with

    - :math:`r_{\rm e}`: scale radius, :math:`[r_{\rm e}] ={\rm kpc}`, which contains half the mass of the halo,
    -  :math:`\rho_{\rm e}`: normalisation, :math:`[\rho_{\rm e}] = {\rm M}_\odot\,{\rm kpc}^{-3}`. With the above definition, we  have :math:`\rho(r_{\rm s}) = \rho_{\rm e}`,
    - :math:`n`: shape parameter (required to be an integer in :program:`CLUMPY`). :raw-html:`<br>`



Burkert (:keyword:`kBURKERT`)
+++++++++++++++++++++++++++++

The Burkert profile :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/1995ApJ...447L..25B target="_blank"> (Burkert, 1995)</a>` is a phenomenological density distribution that resembles an isothermal profile in the inner regions (:math:`r\ll r_0`), and with an outer logarithmic slope of :math:`-3` as in a NFW profile:

  .. math::
       \rho\left(r\,|\,r_{0},\rho_{0}\right)= \frac{\rho_0}{\left(1+\frac{r}{r_0}\right) \times \left[1+\left(\frac{r}{r_0}\right)^2\right]}\;.


For this profile, :math:`\rho(r_0) = \rho_0/4`., and :math:`r_{-2}` is the root of :math:`x^3-x-2=0` (with :math:`x=r_{-2}/r_0`,  i.e. :math:`r_{-2} \approx 1.5213797068 \times r_0`) with

  - :math:`r_0`: scale (or core) radius, :math:`[r_{0}] ={\rm kpc}`,
  - :math:`\rho_0`: core density :math:`\rho(r=0)`,  :math:`[\rho_{0}] = {\rm M}_\odot\,{\rm kpc}^{-3}`. :raw-html:`<br>`


.. _rst_prob_distributions: 

Special :math:`{\rm d}\mathcal{P}/{\rm d}V` profiles
+++++++++++++++++++++++++++++++++++++++++++++++++++++

Besides the description of halo and subhalo profiles, we also categorise as *profile* the spatial distribution :math:`{\rm d}\mathcal{P}/{\rm d}V` of subhaloes in their host halo.

   - **Anti-biased Einasto** (:keyword:`kDPDV_SPRINGEL08_ANTIBIASED`)

     Although not being a strict member of the family :eq:`eq_einasto`, a so-called *anti-biased* Einasto profile is sometimes used to described the population of substructures  in its host halo (e.g., the Milky Way), where tidal effects tend to destroy haloes in the vicinity of the (Galactic) centre, hence the name anti-biased :raw-html:`<a href=http://adsabs.harvard.edu/abs/2008MNRAS.391.1685S target="_blank"> (Springel et al., 2008)</a>.` In practice, this amounts to a simple multiplication by :math:`r/r_{-2}`, such that

      .. math::
            \rho^{\rm DPDV\_SPRINGEL08\_ANTIBIASED}\left(r\,|\,r_{\rm e},\rho_{\rm e};\alpha\right) &= \left(\frac{r}{r_{\rm e}}\right) \times \rho^{\rm EINASTO}(r) \\
               &= \left(\frac{r}{r_{\rm e}}\right) \times \rho_{\rm e} \exp\left\{-\frac{2}{\alpha}\left[\left(\frac{r}{r_{\rm e}}\right)^\alpha -1\right]\right\}\;.

     With this profile, we have we have :math:`r_{-2} = \left(\frac{3}{2}\right)^{1/\alpha} r_{\rm e}`, with

      - :math:`r_{\rm e}`: scale radius, :math:`[r_{\rm e}] ={\rm kpc}`, for which the slope for the *standard* Einasto profile's slope is :math:`-2`,
      -  :math:`\rho_{\rm e}`: normalisation, :math:`[\rho_{\rm e}] = {\rm M}_\odot\,{\rm kpc}^{-3}`. With the  definition of the standard Einasto profile, we still have :math:`\rho(r_{\rm s}) = \rho_{\rm e}`,
      - :math:`\alpha`: shape parameter as for the standard Einasto profile. :raw-html:`<br>`


   - **Gao** :math:`(ac,\,\alpha,\beta)` **profile** (:keyword:`kDPDV_GAO04`)

     The fit function from  :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/2004MNRAS.355..819G target="_blank">Gao et al., (2004)</a>, see also <a href=http://cdsads.u-strasbg.fr/abs/2008ApJ...679.1260M target="_blank"> Madau et al. (2008)</a>,` with parameters :math:`a`, :math:`\alpha`, and :math:`\beta` is used to model the distribution of :math:`N` subclumps in a given host halo of concentration :math:`c(M)` and scale radius :math:`r_{200}`. Note that these authors give

        .. math::

         N(<x) = \frac{(1+ac)\,x^\beta}{1+ac x^\alpha}, \quad {\rm with} \quad x=\frac{r}{r_{200}},

     so that using :math:`N(<x) = \int r^2 \rho(r)\,{\rm d}r`, with :math:`\rho(r)` the profile we are looking for, we end up with

        .. math::

            \rho^{\rm GAO}\left(r\,|\,r_{200},\rho_{200};ac,\alpha,\beta\right)= \frac{\rho_{200}}{\beta-\alpha\frac{ac}{1+ac}}\times \frac{ (1+ac) \left(\frac{r}{r_{200}}\right)^{\beta -3} \left[\beta + ac (\beta - \alpha)  \left(\frac{r}{r_{200}}\right)^\alpha\right]}{\left[1+ ac \left(\frac{r}{r_{200}}\right)^\alpha\right]^2}.

     Given this choice of normalisation, we have :math:`r_{-2}` the root of a second order polynomial involving :math:`(ac,\alpha,\beta)`, with

      - :math:`r_{200}`: scale radius, :math:`[r_{200}] ={\rm kpc}`,
      - :math:`\rho_{200}`: normalisation :math:`[\rho_{200}]={\rm M}_\odot\,{\rm kpc}^{-3}`, :math:`\rho_{200}=\rho(r_{200}) = \rho_{200}=200\times \rho_{\rm m}(z)`, 200 times the mean background matter density,
      - :math:`ac`: first 'combined' parameter of the Gao function: :math:`a` is a parameter, :math:`c` is the concentration of the host halo,
      - :math:`\alpha`:second parameter of the Gao fit (inner slope is :math:`\beta-3`),
      - :math:`\beta`: third parameter of the Gao fit (outer slope is :math:`\beta-3-\alpha`). :raw-html:`<br>`

.. _rst_kNODES: 

Numerical (:keyword:`kNODES`)
+++++++++++++++++++++++++++++

Apart from the previous analytic profiles, also arbitrary numerically defined profiles can be chosen for the halo density or :math:`{\rm d}\mathcal{P}/{\rm d}V` descriptions. This is done by choosing the generic :keyword:`kNODES` keyword. In this case, :program:`CLUMPY` requests an ASCII file, parsed with the option :option:`gLIST_HALOES_NODES`, which contains the numerical profiles of *one or several* halo descriptions:

      - The file must contain two columns separated by spaces or tabs (additional columns are ignored), containing the node points on the radial axis (1st column) and corresponding profile density values (2nd column). The units of both axes can be arbitrary; however, they can be used if wished so (set normalisation density to :keyword:`-1` in the (halo) definition file, see :numref:`rst_halofile_format` or :numref:`rst_enumerators_dm_profiles` for details).
      - Node points (1st column) must be in strictly monotonic increasing order for each object. 
      - Different profiles can be provided one after the other. A break in the monotonic node order is used to identify the separate profiles. Each profile is later chosen by an integer value (starting from 1) for the first shape parameter (":math:`\alpha`").
      - Blank lines and lines starting with ``#`` are ignored.
      - At least four node points must be provided per profile.
      - The log-slope of the profile must be :math:`{\frac{{\rm d}\log \rho}{{\rm d}\log r}=-2}` (see :numref:`rst_r2`) somewhere in the range of node points, and this log-slope also should be unique. Note that this condition is independent of the input units of both columns.

Note that this format corresponds to the output files of :math:`\tt -g1` (:numref:`rst_g1`) and :math:`\tt -h1` (:numref:`rst_h1`). An example node points list is provided in :download:`$CLUMPY/data/list_generic_nodes.txt <../data/list_generic_nodes.txt>`.

.. warning::

   At radii smaller than the first provided node, halo densities are set to a constant core with the value at the first node. It is recommend to check whether the result is stable for the given nodes depending on the use case. If necessary, the user has to model some extrapolation of the innermost cusp by her/himself externally beforehand.

Popular :math:`(\rho_s,r_s)` values
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Dark matter simulations usually provide :math:`\rho_{\rm tot} (r)` from the total density of DM measured in shells and :math:`{\rm d}{\cal P}/{\rm d}V`, the spatial distribution of subhaloes within a halo. The above families of profiles can be used to describe both the total DM density of a halo and the spatial distribution of subhaloes in a host. Remember however that they will come with different values of their structural parameters :math:`r_{\rm s}`, :math:`\rho_{\rm s}`, etc.

In :program:`CLUMPY`, we generically call :math:`r_{\rm s}` the scale radius appearing in the different profiles. However, depending on the chosen parameterisation, it can mean :math:`r_{\rm s}`, :math:`r_{-2}` or :math:`r_{\rm e}` (see :numref:`rst_doc_dm_families`). It is up to the user to check that the values for :math:`r_{\rm s}` she/he uses in the input parameter file are **realistic**. In the same spirit, there are up to three shape parameters denoted :math:`\alpha`, :math:`\beta`, and :math:`\gamma`, although only :math:`\alpha` is used for :keyword:`kEINASTO`, :keyword:`kDPDV_SPRINGEL08_ANTIBIASED`, and :keyword:`kEINASTO_N` (respectively, :math:`n` for the latter), and none for :keyword:`kBURKERT`. :numref:`tab_profile_params` provides several shape parameters proposed in the literature. They also give some values as found from simulations for the scale parameter for the Galaxy (or Galactic-type halos).

.. warning::
  For your science study, copy and paste the values from :numref:`tab_profile_params` with caution!




.. _tab_profile_params:

.. table:: Popular parameter choices for various profile families and their special names

     ========================================================================================================================================== ============== ============== ============== =========================================================================================================================== ================================================= ===============================================================
     **CLUMPY keyword (Popular Name)**                                                                                                          :math:`\alpha` :math:`\beta`  :math:`\gamma` **References**                                                                                                              :math:`r^{\rm Gal.~total~density}_{\rm s}`        :math:`r^{{\rm Gal.}~{\rm d}P/{\rm d}V}_{\rm s}`
     ------------------------------------------------------------------------------------------------------------------------------------------ -------------- -------------- -------------- --------------------------------------------------------------------------------------------------------------------------- ------------------------------------------------- ---------------------------------------------------------------
     :keyword:`kZHAO` (Isothermal)                                                                                                              :math:`2`      :math:`2`      :math:`0`      :math:`-`                                                                                                                   :math:`4\,\rm kpc` [#f1]_                         :math:`28\,\rm kpc` [#f1]_
     :keyword:`kZHAO` (NFW)                                                                                                                     :math:`1`      :math:`3`      :math:`1`      :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/1997ApJ...490..493N target="_blank">Navarro, Frenk & White (1997) </a>`   :math:`21.7\,\rm kpc` [#f2]_                      :math:`-`
     :keyword:`kZHAO` (Moore)                                                                                                                   :math:`1.5`    :math:`3`      :math:`1.5`    :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/1998ApJ...499L...5M target="_blank">Moore et al. (1998)</a>`              :math:`34.5\,\rm kpc` [#f2]_                      :math:`-`
     :keyword:`kZHAO` (DMS04)                                                                                                                   :math:`1`      :math:`3`      :math:`1.2`    :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/2004MNRAS.353..624D target="_blank">Diemand, Moore & Stadel (2004)</a>`   :math:`32.6\,\rm kpc` [#f2]_                      :math:`-`
     :keyword:`kBURKERT`                                                                                                                        :math:`-`      :math:`-`      :math:`-`      :raw-html:`<a href=http://adsabs.harvard.edu/abs/2013JCAP...07..016N target="_blank">Nesti and Salucci (2013)</a>`          :math:`-`                                         :math:`9.26\,\rm kpc`
     :keyword:`kEINASTO_N`                                                                                                                      :math:`6`      :math:`-`      :math:`-`      :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/2006AJ....132.2685M target="_blank">Merritt et al. (2006)</a>`            :math:`\sim 250\,\rm kpc`                         :math:`-`
     :keyword:`kEINASTO`                                                                                                                        :math:`0.17`   :math:`-`      :math:`-`      :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/2004MNRAS.349.1039N target="_blank">Navarro et al. (2004)</a>`            :math:`21.7\,\rm kpc`                             :math:`-`
     :keyword:`kEINASTO`                                                                                                                        :math:`0.68`   :math:`-`      :math:`-`      :raw-html:`<a href=http://adsabs.harvard.edu/abs/2008MNRAS.391.1685S target="_blank">Springel et al. (2008)</a>`            :math:`-`                                         :math:`199\,\rm kpc`
     :keyword:`kDPDV_GAO04`                                                                                                                        :math:`19`     :math:`1.9`    :math:`2.9`    :raw-html:`<a href=http://cdsads.u-strasbg.fr/abs/2008ApJ...679.1260M target="_blank">Madau et al. (2008)</a>`              :math:`-`                                         :math:`389\,\rm kpc`
     ========================================================================================================================================== ============== ============== ============== =========================================================================================================================== ================================================= ===============================================================


.. [#f1] Values from :raw-html:`<a href=http://adsabs.harvard.edu/abs/2008A%26A...479..427L target="_blank">Lavalle et al. (2008)</a>.`
.. [#f2] Values from :raw-html:`table II of <a href=http://adsabs.harvard.edu/abs/2004PhRvD..70j3529F target="_blank">Fornengo, Pieri & Scopel (2004)</a>.`





Plots :math:`\rho^2(r)`, :math:`r^2\rho(r)`,...
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

The following figures display a comparison of the various profiles in the central regions of the halos. The saturation density is reached for some of them :math:`(1\,\rm GeV\,cm^{-3} = 2.63 \times 10^7~{\rm M}_\odot\,{\rm kpc}^{-3})`.


.. _fig_rho2:

.. figure:: DocImages/rho2.png
   :align: center
   :figclass: align-center
   :scale: 33%

   DM smooth profile squared :math:`\rho^2(r)` (normalised to :math:`\rho_0 = 0.3~\rm GeV\,cm^{-3}` at :math:`r=8\,\rm kpc`).


.. _fig_r2rho:

.. figure:: DocImages/r2rho.png
   :align: center
   :figclass: align-center
   :scale: 33%

   Function :math:`{\rm d}M/{\rm d}r \propto r^2\rho(r)\; \Rightarrow\;` mass :math:`M =4\pi\int r^2 \rho(r)\, {\rm d}r`.


.. _fig_r2rho2:

.. figure:: DocImages/r2rho2.png
   :align: center
   :figclass: align-center
   :scale: 33%

   Function :math:`{\rm d}{\cal L}/{\rm d}r \propto r^2\rho^2(r) \; \Rightarrow\;` intrinsic luminosity :math:`{\cal L} = 4\pi\int r^2 \rho(r)^2 \,{\rm d}r`.


.. note::

   To calculate the mass :math:`M` and luminosity :math:`{\cal L}` of a clump, the functions shown in the last two previous plots need to be integrated. If no analytical solution exists, we split the integration into several parts to speed up the calculation:

   .. math::

       I \equiv \int\limits_0^{R_{\Delta}} f(r)\;{\rm d}r\; = \int\limits_0^{r=r_{\rm sat}} f(r)\;{\rm d}r \;\;+ \int\limits_{r_{\rm sat}}^{r=0.01\,r_{\rm s}} f(r)\;{\rm d}r\;\; + \int\limits_{r=0.01\,r_{\rm s}}^{R_{\Delta}} f(r)\;{\rm d}r\,.

   The first part  is integrated with a linear step integrator, and the second and third parts  are integrated in adaptive logarithmic steps.

