.. _rst_doc_module_z:

Compute spectra: :math:`\tt -z`
--------------------------------

Simply compute :math:`\gamma`-ray or neutrino spectra at source:

.. code-block:: console

    $ clumpy -z -D
    $ clumpy -z -i clumpy_params_z.txt


.. figure:: DocImages/zD.png
   :align: center
   :figclass: align-center
   :scale: 55%

   :option:`-z`  output :program:`ROOT` figure


.. seealso:: :numref:`rst_physics_spectra`, :ref:`rst_physics_spectra`, for details.