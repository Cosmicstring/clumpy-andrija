.. _rst_enumerators_cdelta:

.. role::  raw-html(raw)
    :format: html

Mass-concentration relations
--------------------------------------------------------------

Possible keyword choices for :option:`gXXX_FLAG_CDELTAMDELTA` variables

.. warning:: 

     Be aware that different :math:`c_\Delta-M_\Delta` parametrisations are only valid for restricted mass scales, which are given in the table below.
     

.. note:: Various :math:`c_\Delta-M_\Delta` parametrisations use the same fitting formula from :raw-html:`<a href="http://adsabs.harvard.edu/abs/2008A%26A...479..427L" target="_blank">Lavalle et al. (2008)</a>`,

         .. math::

              \ln (c_{\Delta} ) =  \sum_{i=0}^{n} C_i \cdot \left[  \ln\left(\frac{M_{\Delta}}{M_\odot}\right) \right]^{i} + \ln\left(\frac{1}{1+z}\right)\,.

         In these cases, we provide the :math:`C_i` coefficients in the table below.

.. csv-table:: 
   :file: tables/gENUM_CDELTAMDELTA.csv_table
   :widths: 10, 30
   :align: left


:raw-html:`<br>&nbsp;<br>`

.. seealso::
   :numref:`rst_physics_concentration`; see :numref:`fig_cdelta` for a comparison of the various concentration-mass relations.

