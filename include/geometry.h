#ifndef _CLUMPY_GEOMETRY_H_
#define _CLUMPY_GEOMETRY_H_

// C++ std libraries
#include <iostream>
#include <math.h>
#include <stdlib.h>
using namespace std;

double angular_dist(double const &dec1_rad, double const &dec2_rad, double const &ra1_rad, double const &ra2_rad); //!< Returns angular distance [rad] from declinations and right ascensions of two objects (in radians) using the ‘haversine’ formula.
void   check_psi(double &psi_rad); //!< Enforces angle in range -pi<psi<pi [rad]
double distance_from_angular_dist(double const &alpha_rad, double const &d_kpc); //!<  Returns separation between two objects at the same distance d from their angular distance [kpc]
void   euler_rotation(double x[3], double euler_deg[3]); //!< Returns new coordinates x_new[0...2] for input point x[0...2] after rotation along the three Euler angles (alpha,beta,gamma)_Euler [deg].
void   greatcircle_intermediate_points(double const &psi1_rad, double const &theta1_rad,
                                       double const &psi2_rad, double const &theta2_rad,
                                       double const &dist_frac,
                                       double &psi_res_rad, double &theta_res_rad); //!< Returns the coordinates of a point on a great circle defined by the two points (psi1, theta1) and (psi2, theta2) at a distance dist_frac * dist, from (psi1, theta1). Dist is the distance between (psi1, theta1) and (psi2, theta2).
void   lalphabeta_to_lpsitheta(double x[3], double par[2]);//!< Returns (l,psi,theta) from (l,alpha,beta) [kpc,rad,rad].
void   lalphabeta_to_rpsitheta(double x[3], double par[2]);//!< Returns (r,psi,theta) from (l,alpha,beta) [kpc,rad,rad].
void   lalphabeta_to_xyzgal(double x[3], double par[2]);//!< Returns (x,y,z)_gal [kpc] from (l,alpha,beta) [kpc,rad,rad].
void   lalphabeta_to_xyzlos(double x[3], double par[2], double const &d_kpc); //!<  Returns (x,y,z)_{halo centre} [kpc] from (l,alpha,beta) [kpc,rad,rad] and observation angle (phi,theta) [rad] from the halo centre distance [kpc].
double philos_to_lmax(double const &phi_los_rad);//!< Returns boundary of the galactic DM halo [kpc] in the l.o.s direction phi_los_rad [rad].
double psitheta_to_phi(double const &psi_rad, double const &theta_rad); //!< Returns phi [rad], the angle between the GC and the l.o.s. direction (from Earth).
void   xyzgal_to_lalphabeta(double x[3], double par[2]); //!< Returns (l,alpha,beta) [kpc,rad,rad] from Gal. cart. coordinates (x,y,z) [kpc].
void   xyzgal_to_lpsitheta(double x[3]); //!< Returns (l,psi,theta) [kpc,rad,rad] from Gal. cart. coordinates (x,y,z) [kpc].
void   xyzgal_to_rpsitheta(double x[3]); //!< Returns (r,psi,theta) [kpc,rad,rad] from Gal. cart. coordinates (x,y,z) [kpc].

#endif

/*! \file geometry.h
  \brief Geometric transformations: spherical to cartesian Galactic coord. (and vice-versa).
*/
