/*! \file integr.cc \brief (see integr.h) */

// CLUMPY includes
#include "../include/integr.h"
#include "../include/misc.h"

// C++ std libraries
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <cstdio>
using namespace std;

//______________________________________________________________________________
void simpson_lin_adapt(void fn(double &, double *, double &), double &xmin, double &xmax,
                       double par[], double &s, double const &eps, bool is_verbose)
{
   //--- Returns 1D integral of fn using Simpson rule in linear steps.
   //    The doubling of the steps stops when the precision 'eps' is
   //    reached (adapted from numerical recipes).
   //
   // INPUTS:
   //  fn            Function to integrate
   //  xmin          Lower integration limit
   //  xmax          Upper integration limit
   //  par[]         Parameters for fn()
   //  eps           Relative precision sought
   //  is_verbose    Chatter on integration
   // OUTPUT:
   //  s             Result of the integration

   double ost = -1.e-40;
   double os = -1.e-40;
   int jmax = 25;
   double st = 0.;
   if (xmin >= xmax) {
      s = 0.;
      return;
   }
   for (int j = 1; j <= jmax; ++j) {
      trapeze_lin_refine(fn, xmin, xmax, par, st, j);
      s = (4.*st - ost) / 3.;
      if ((fabs(s - os) < eps * fabs(os) || s < 1.e-30) && j >= 3) {
         if (is_verbose) cout << ">>>>> simpson_lin_adapt: convergence Dres/res<"
                                 << eps << " reached after "
                                 << j << " refinement (i.e. "
                                 << pow(2, j) << " steps)" << endl;
         return;
      }
      os = s;
      ost = st;
   }

   print_error("integr.cc", "simpson_lin_adapt()", "Too many steps (not converged)!");

}

//______________________________________________________________________________
void simpson_log_adapt(void fn(double &, double *, double &), double &xmin, double &xmax,
                       double par[], double &s, double const &eps, bool is_verbose)
{
   //--- Returns 1D integral of fn using Simpson rule in logarithmic steps.
   //    The doubling of the steps stops when the precision 'eps' is reached
   //    (adapted from numerical recipes).
   //
   // INPUTS:
   //  fn            Function to integrate
   //  xmin          Lower integration limit
   //  xmax          Upper integration limit
   //  par[]         Parameters for fn()
   //  eps           Relative precision sought
   //  is_verbose    Chatter on integration
   // OUTPUT:
   //  s             Result of the integration

   double ost = -1.e-40;
   double os = -1.e-40;
   int jmax = 25;
   double st = 0.;
   if (xmin >= xmax) {
      s = 0.;
      return;
   }

   for (int j = 1; j <= jmax; ++j) {
      trapeze_log_refine(fn, xmin, xmax, par, st, j);
      s = (4.*st - ost) / 3.;
      if ((fabs(s - os) < eps * fabs(os) || s < 1.e-30) && j >= 3) {
         if (is_verbose) cout << ">>>>> simpson_log_adapt: convergence Dres/res<"
                                 << eps << " reached after "
                                 << j << " refinement (i.e. "
                                 << pow(2, j) << " steps)" << endl;
         return;
      }
      os = s;
      ost = st; 
   }
   print_error("integr.cc", "simpson_log_adapt()", "Too many steps (not converged)!");
}

//______________________________________________________________________________
void simpson_log(void fn(double &, double *, double &), double &xmin, double &xmax,
                 double par[], double &s, int nstep)
{
   //--- Returns 1D integral of fn with 'nsteps' log steps with Simpson rule
   //    (adapted from numerical recipes).
   //
   // INPUTS:
   //  fn            Function to integrate
   //  xmin          Lower integration limit
   //  xmax          Upper integration limit
   //  par[]         Parameters for fn()
   //  nstep         Number of steps used for the integration
   // OUTPUT:
   //  s             Result of the integration

   if (xmin <= 0.) {
      print_error("integr.cc", "simpson_log()", "Log step with xmin=0 is forbidden.");
   }

   double log_step = pow(xmax / xmin, 1. / (double(2 * nstep)));
   // We take 2*nstep (to ensure an even number of steps in total)
   // Simpson steps are 1/3, 4/3, 2/3, 4/3,...., 4/3, 1/3

   // First and last step (simpson =1/3)
   // multiplied by "x" because of the logarithmic step
   double fn_res = 0.;
   fn(xmin, par, fn_res);
   s += fn_res * xmin;
   fn(xmax, par, fn_res);
   s += fn_res * xmax;

   // First 4/3 step
   double x = xmin * log_step;
   fn(x, par, fn_res);
   s +=  4. * fn_res * x;

   // All remaining 2/3 and 4/3 steps
   for (int i = 1 ; i < nstep; i++) {
      // Even step
      x *= log_step;
      fn(x, par, fn_res);
      s += 2. * fn_res * x;
      // Odd step
      fn(x, par, fn_res);
      x *= log_step;
      s += 4. * fn_res * x;
   }
   s *= log(log_step) / 3.;
   return;
}

//______________________________________________________________________________
void trapeze_lin_refine(void fn(double &, double *, double &), double &xmin,
                        double &xmax, double par[], double &s, int n)
{
   //--- Never call this function directly! It must always be called by
   //    simpson_lin_adapt()! Allows for the doubling of the step with the
   //    trapezium rule (adated from numerical recipes).
   //
   // INPUTS:
   //  fn            Function to integrate
   //  xmin          Lower integration limit
   //  xmax          Upper integration limit
   //  par[]         Parameters for fn()
   //  n             Number of steps
   // OUTPUT:
   //  s             Result of the integration

   if (n == 1) {
      double fn_res = 0.;
      fn(xmin, par, fn_res);
      s += fn_res;
      fn(xmax, par, fn_res);
      s += fn_res;
      s *= 0.5 * (xmax - xmin);
   } else {
      int tnm = (int)pow(2, n - 2);
      double del = (xmax - xmin) / (double)tnm;
      double r = xmin + 0.5 * del;
      double sum = 0.0;
      for (int j = 1; j <= tnm; ++j) {
         double fn_res;
         fn(r, par, fn_res);
         sum += fn_res;
         r += del;
      }
      s = 0.5 * (s + del * sum);
   }
   return;
}

//______________________________________________________________________________
void trapeze_log_refine(void fn(double &, double *, double &), double &xmin,
                        double &xmax, double par[], double &s, int n)
{
   //--- Never call this function directly! It must always be called by
   //    simpson_log_adapt()! Allows for the doubling of the step with the
   //    trapezium rule (adated from numerical recipes).
   //
   // INPUTS:
   //  fn            Function to integrate
   //  xmin          Lower integration limit
   //  xmax          Upper integration limit
   //  par[]         Parameters for fn()
   //  n             Number of steps
   // OUTPUT:
   //  s             Result of the integration

   if (xmin <= 0.) {
      print_error("integr.cc", "trapeze_log_refine()", "Log step with xmin=0 is forbidden.");
   }

   if (n == 1) {
      double fn_res = 0.;
      fn(xmin, par, fn_res);
      s += fn_res * xmin;
      fn(xmax, par, fn_res);
      s += fn_res * xmax;
      s *= 0.5 * log(xmax / xmin);
   } else {
      double tnm = (int)pow(2, n - 2);
      double del =  pow(xmax / xmin, 1. / (double)tnm);
      double r = xmin * sqrt(del);
      double sum = 0.0;
      for (int j = 1; j <= tnm; ++j) {
         double fn_res;
         fn(r, par, fn_res);
         sum += fn_res * r;
         r *= del;
      }
      s = 0.5 * (s +  log(xmax / xmin) * sum / (double)tnm);
   }
   return;
}
