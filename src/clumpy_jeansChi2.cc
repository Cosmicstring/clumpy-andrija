// C++ std libraries
#include <stdlib.h>

// GreAT includes
#include "../include/clumps.h"
#include "../include/geometry.h"
#include "../include/inlines.h"
#include "../include/integr.h"
#include "../include/integr_los.h"
#include "../include/janalysis.h"
#include "../include/misc.h"
#include "../include/params.h"
#include "../include/profiles.h"
#include "../include/stat.h"
#include "../include/jeans_analysis.h"

// ROOT includes
#if IS_ROOT
#include "Math/Minimizer.h"
#include "Math/Factory.h"
#include "Math/Functor.h"
#endif


const int n_stat_params = 9; //number of possible free parameters
const int n_jeans_param = 20; // number of jeans parameters

string names_jeans_params [n_jeans_param] = {"log10(rho_s)", "log10(r_s)", "alpha", "beta", "gamma", "DM_profile", "Rvir", "rho_s*", "r_s*", "alpha*", "beta*", "gamma*", "Light_profile", "R", "eps", "beta_0", "beta_infinity", "log10(r_a)", "eta", "Aniso_profile"};
double param[n_jeans_param]; // initial values + needed values for jeans analysis

struct gStructJeansAnalysis stat_analysis; // structure containing the Jeans parameters (filled from a Jeans analysis parameters file)
struct gStructHalo stat_halo, halo_ref; // structure containing the kinematic data (filled from a Jeans kinematic data file)

double vmean = 0.; // mean velocity of the object (for unbinned analysis)
int type_analysis; // 0: binned with data = sigmap2; 1: binned with data = sigmap; 2: unbinned
double eps; // precision

bool is_verbose = false;

//______________________________________________________________________________
void SetParameters()    // initialization of the parameters
{
   // Set the values of the parameters from the Jeans analysis file

   //  param[0]     Dark matter density normalisation: density [Msol/kpc^3]
   //  param[1]     Dark matter density Scale radius [kpc]
   //  param[2]     Dark matter Shape parameter #1
   //  param[3]     Dark matter Shape parameter #2
   //  param[4]     Dark matter Shape parameter #3
   //  param[5]     Dark matter card_profile [gENUM_PROFILE]
   //  param[6]     Max radius of integration
   //  param[7]     Normalisation of Light profile [Lsol]
   //  param[8]     Light profile scale radius [kpc]
   //  param[9]     Light profile Shape parameter #1
   //  param[10]    Light profile Shape parameter #2
   //  param[11]    Light profile Shape parameter #3
   //  param[12]    Light profile card_profile [gENUM_LIGHTPROFILE] for Light Profile
   //  param[13]    R : projected radius considered for calculation of a projected quantity (kpc) // unused
   //  param[14]    eps : precision for integrations
   //  param[15]    Anisotropy at r=0
   //  param[17]    Anisotropy at r=+infinity
   //  param[17]    Scale radius [kpc]
   //  param[18]    Shape parameter
   //  param[19]    card_profile [gENUM_ANISOTROPYPROFILE] for Anisotropy Profile

   param[0] = stat_analysis.Value[0];
   param[1] = stat_analysis.Value[1];
   param[2] = stat_analysis.Value[2];
   param[3] = stat_analysis.Value[3];
   param[4] = stat_analysis.Value[4];
   param[5] = stat_analysis.HaloProfile;
   param[6] = stat_analysis.HaloSize * 100.;
   param[7] = stat_analysis.LightParam[0];
   param[8] = stat_analysis.LightParam[1];
   param[9] = stat_analysis.LightParam[2];
   param[10] = stat_analysis.LightParam[3];
   param[11] = stat_analysis.LightParam[4];
   param[12] = stat_analysis.LightProfile;
   param[13] = 100.; // unused
   param[14] = eps;
   param[15] = stat_analysis.Value[5];
   param[16] = stat_analysis.Value[6];
   param[17] = stat_analysis.Value[7];
   param[18] = stat_analysis.Value[8];
   param[19] = stat_analysis.AnisoProfile;
   return;
}

//______________________________________________________________________________
double chi2sigmap(const double *par)
{
   // Chi square function used for binned analyses (either using sigmap or sigmap2 data)

   // Copy parameters proposed by the minimizer
   double par_tmp[n_jeans_param];
   for (int i = 0; i < 5; ++i)
      par_tmp[i] = par[i];
   for (int i = 5; i < 9; ++i)
      par_tmp[i + 10] = par[i];
   for (int i = 5; i < 15; ++i)
      par_tmp[i] = par[i + 4];
   par_tmp[19] = par[19];

   // if proposed parameters are nan: skip
   for (int i = 0; i < n_jeans_param; ++i) {
      if (std::isnan(par_tmp[i])) {
         cout << "--- skip nan values" << endl;
         return 1000.;
      }
   }
   // if proposed parameters are out of range: skip
   for (int k = 0; k < n_stat_params; k++) {
      if (stat_analysis.IsFreeParam[k] == 1) {
         if (par[k] < stat_analysis.LowerRange[k] || par[k] > stat_analysis.UpperRange[k])
            return 1000.;
      }
   }

   // put parameters back to linear scale
   par_tmp[0] = pow(10., par_tmp[0]);
   par_tmp[1] = pow(10., par_tmp[1]);
   par_tmp[17] = pow(10., par_tmp[17]);

   return -2.*log_likelihood_jeans_binned(par_tmp, stat_halo, type_analysis);
}

//______________________________________________________________________________
double chi2vel(const double *par)
{
   // Chi square function used for unbinned analyses

   // Copy parameters proposed by the minimizer
   double par_tmp[n_jeans_param];
   for (int i = 0; i < 5; ++i)
      par_tmp[i] = par[i];
   for (int i = 5; i < 9; ++i)
      par_tmp[i + 10] = par[i];
   for (int i = 5; i < 15; ++i)
      par_tmp[i] = par[i + 4];
   par_tmp[19] = par[19];

   // if proposed parameters are nan: skip
   for (int i = 0; i < n_jeans_param; ++i) {
      if (std::isnan(par_tmp[i])) {
         cout << "--- skip nan values" << endl;
         return 1000.;
      }
   }
   // if proposed parameters are out of range: skip
   for (int k = 0; k < n_stat_params; k++) {
      if (stat_analysis.IsFreeParam[k] == 1) {
         if (par[k] < stat_analysis.LowerRange[k] || par[k] > stat_analysis.UpperRange[k])
            return 1000.;
      }
   }
   // put parameters back to linear scale
   par_tmp[0] = pow(10., par_tmp[0]);
   par_tmp[1] = pow(10., par_tmp[1]);
   par_tmp[17] = pow(10., par_tmp[17]);

   return -2.*log_likelihood_jeans_unbinned(par_tmp, stat_halo, vmean);
}

#if IS_ROOT
//______________________________________________________________________________
void Minimise_jeans(int choice, int n_bootstrap, string output)
{
   // Choice: 1: chi2 minimization; 2: bootstrap

   // Create minimiser
   //    N.B.: the minimiser is defined by its name, its algorithm (optional),
   //          and several other parameters loaded from the par file. Possible
   //          combinations for {minimiser, algorithm} are:
   //          Name               Name algorithm
   //      Minuit /Minuit2        Migrad, Simplex,Combined,Scan  (default is Migrad)
   //      Minuit2                Fumili2
   //      Fumili                    -
   //      GSLMultiMin            ConjugateFR, ConjugatePR, BFGS,
   //                             BFGS2, SteepestDescent
   //      GSLMultiFit               -
   //      GSLSimAn                  -
   //      Genetic                   -
   ROOT::Math::Minimizer *minimiser = ROOT::Math::Factory::CreateMinimizer("Minuit", "Migrad");

   // Set minimiser parameters (tolerance , etc)
   minimiser->SetMaxFunctionCalls(1000000); // for Minuit/Minuit2
   minimiser->SetMaxIterations(10000);  // for GSL
   minimiser->SetTolerance(0.001);
   minimiser->SetPrecision(1.e-8);
   minimiser->SetPrintLevel(0);

   // Create function wrapper for minimiser (IMultiGenFunction type):
   ROOT::Math::Functor f_sigmap(&chi2sigmap, n_jeans_param);
   ROOT::Math::Functor f_vel(&chi2vel, n_jeans_param);

   // Set the analysis type (binned, unbinned)
   if (stat_halo.JeansData.TypeData == "Sigmap2" || stat_halo.JeansData.TypeData == "Sigmap") {
      if (choice == 2) { // bootstrap analysis
         cout << "Error: no bootstrap analysis is allowed for a binned Jeans analysis: abort" << endl;
         abort();
      } else {
         cout << "======== Binned Jeans analysis ========" << endl;
         if (stat_halo.JeansData.TypeData == "Sigmap2")
            type_analysis = 0;
         else if (stat_halo.JeansData.TypeData == "Sigmap")
            type_analysis = 1;
      }
   } else if (stat_halo.JeansData.TypeData == "Vel") {
      cout << "======== Unbinned Jeans analysis ========" << endl;
      type_analysis = 2;
      // Compute mean velocity for unbinned analysis
      double termup = 0;
      double termdown = 0.;
      for (int k = 0; k < int(stat_halo.JeansData.Radius.size()); k++) {
         termup += stat_halo.JeansData.MembershipProb[k] * stat_halo.JeansData.VelocData[k] / pow(stat_halo.JeansData.ErrVelocData[k], 2);
         termdown += stat_halo.JeansData.MembershipProb[k] / pow(stat_halo.JeansData.ErrVelocData[k], 2);
      }
      vmean = termup / termdown;
      if (is_verbose)
         cout << "vmean = " << vmean << endl;
   }

   // Set the likelihood function
   if (type_analysis == 0 || type_analysis == 1) //Sigmap keyword in the data file
      minimiser->SetFunction(f_sigmap);
   else if (type_analysis == 2) //Vel keyword in the data file
      minimiser->SetFunction(f_vel);

   int Nfreepar = 0; // number of free parameters (used only for statistical file header)
   double step = 0.01; // Step size
   // Loop on possible free parameters
   for (int i = 0; i < n_stat_params; ++i) {
      if (stat_analysis.IsFreeParam[i] == 1)  // Free parameters
         Nfreepar += 1;
   }

   // Create output file
   FILE *fp = fopen(output.c_str(), "w");
   // Create header
   fprintf(fp, "Name     long.(deg)  lat.(deg)  l(kpc)  Ndata  Npar Rmax\n");
   fprintf(fp, "%s         %le        %le         %le     %d    %d   %le \n\n", stat_analysis.Name.c_str(), stat_analysis.Longitude, stat_analysis.Latitude, stat_analysis.Distance, int(stat_halo.JeansData.Radius.size()), Nfreepar, gSIM_JEANS_RMAX);
   fprintf(fp, "rhos(Msol/kpc3) rs(kpc) Rvir(kpc) profile alpha beta gamma lightprofile L(Lsol) rL(kpc) alpha* beta* gamma* anisoprofile beta0 betainf raniso(kpc) eta chi2\n");

   if (choice == 2) { // Bootstrap analysis
      /* initialize random seed: */
      srand(time(NULL));
      for (int k = 0; k < n_bootstrap; ++k) { // Bootstrap the data
         SetParameters(); // Re-initialize the parameters values
         int n_data = stat_halo.JeansData.Radius.size();
         cout << "===== BOOTSTRAP SAMPLE #" << k << "  (" << n_data << " DATA POINTS)" << endl;

         // Draw n_data indices among n_data
         if (is_verbose)
            cout << "Indices of bootstrap data: ";
         //vector<int> i_bootstrap;
         int l = 0;
         do {
            int index = rand() % n_data;
            if (is_verbose)
               cout << index << "  ";
            // Update stat_halo values from ref and this bootstrap index
            stat_halo.JeansData.Radius[l]       = halo_ref.JeansData.Radius[index];
            stat_halo.JeansData.VelocData[l]    = halo_ref.JeansData.VelocData[index];
            stat_halo.JeansData.ErrVelocData[l] = halo_ref.JeansData.ErrVelocData[index];
            stat_halo.JeansData.MembershipProb[l] = halo_ref.JeansData.MembershipProb[index];
            ++l;
         } while (l < n_data);
         if (is_verbose)
            cout << endl;



         // Update vmean
         double termup = 0;
         double termdown = 0.;
         for (int kk = 0; kk < int(stat_halo.JeansData.Radius.size()); kk++) {
            termup += stat_halo.JeansData.MembershipProb[kk] * stat_halo.JeansData.VelocData[kk] / pow(stat_halo.JeansData.ErrVelocData[kk], 2);
            termdown += stat_halo.JeansData.MembershipProb[kk] / pow(stat_halo.JeansData.ErrVelocData[kk], 2);
            if (is_verbose)
               cout << "pi = " << stat_halo.JeansData.MembershipProb[kk] << endl;
         }
         vmean = termup / termdown;
         if (is_verbose)
            cout << "vmean = " << vmean << endl;


         // Set the free parameters
         for (int j = 0; j < n_stat_params; j++) {
            if (stat_analysis.IsFreeParam[j] == 1) {
               Nfreepar += 1;
               if (j < 5)
                  minimiser->SetLimitedVariable(j, names_jeans_params[j], stat_analysis.Value[j], step, stat_analysis.LowerRange[j], stat_analysis.UpperRange[j]);
               else
                  minimiser->SetLimitedVariable(j, names_jeans_params[j + 10], stat_analysis.Value[j], step, stat_analysis.LowerRange[j], stat_analysis.UpperRange[j]);
            }
         }
         // Set the fixed parameters
         for (int j = 0; j < n_stat_params; j++) {
            if (stat_analysis.IsFreeParam[j] == 0) {
               if (j < 5)
                  minimiser->SetFixedVariable(j, names_jeans_params[j], stat_analysis.Value[j]);
               else
                  minimiser->SetFixedVariable(j, names_jeans_params[j + 10], stat_analysis.Value[j]);
            }
         }

         // Add the other fixed parameters
         for (int j = 0; j < 10; j++)
            minimiser->SetFixedVariable(j + 9, names_jeans_params[j + 5], param[j + 5]);
         minimiser->SetFixedVariable(19, names_jeans_params[19], param[19]);

         // (minimise for each sample)
         minimiser->Minimize();
         double_t chi2_min = minimiser->MinValue();
         if (is_verbose)
            cout << "Minimum: " << minimiser->MinValue()  << std::endl;
         minimiser->PrintResults();

         // Copy best-fit parameters and errors
         const double_t *xi = minimiser->X();
         //const Double_t *err = minimiser->Errors();
         double rhos = pow(10., xi[0]);
         double rs = pow(10., xi[1]);
         double alpha = xi[2];
         double beta = xi[3];
         double gamma = xi[4];
         double beta0 = xi[5];
         double betainf = xi[6];
         double ra = pow(10., xi[7]);
         double eta = xi[8];
         int dm_prof = param[5];
         int light_prof = param[12];
         int ani_prof = param[19];

         // Write parameters values in output file
         fprintf(fp, "%.*le %.*le %.*le %s %.*le %.*le %.*le %s %g %g %g %g %g %s %.*le %.*le %.*le %.*le %.*le\n",
                 gSIM_SIGDIGITS, rhos, gSIM_SIGDIGITS, rs, gSIM_SIGDIGITS, stat_analysis.HaloSize,
                 ("k" + (string)gNAMES_PROFILE[dm_prof]).c_str(),
                 gSIM_SIGDIGITS, alpha, gSIM_SIGDIGITS, beta, gSIM_SIGDIGITS, gamma,
                 ("k" + (string)gNAMES_LIGHTPROFILE[light_prof]).c_str(),
                 param[7], param[8],
                 param[9], param[10], param[11],
                 ("k" + (string)gNAMES_ANISOTROPYPROFILE[ani_prof]).c_str(),
                 gSIM_SIGDIGITS, beta0, gSIM_SIGDIGITS, betainf,
                 gSIM_SIGDIGITS, ra, gSIM_SIGDIGITS, eta, gSIM_SIGDIGITS, chi2_min);
      }
      fclose(fp);
   } else { // Simple minimization

      // Set the free parameters
      for (int j = 0; j < n_stat_params; j++) {
         if (stat_analysis.IsFreeParam[j] == 1) {
            Nfreepar += 1;
            if (j < 5)
               minimiser->SetLimitedVariable(j, names_jeans_params[j], stat_analysis.Value[j], step, stat_analysis.LowerRange[j], stat_analysis.UpperRange[j]);
            else
               minimiser->SetLimitedVariable(j, names_jeans_params[j + 10], stat_analysis.Value[j], step, stat_analysis.LowerRange[j], stat_analysis.UpperRange[j]);
         }
      }
      // Set the fixed parameters
      for (int j = 0; j < n_stat_params; j++) {
         if (stat_analysis.IsFreeParam[j] == 0) {
            if (j < 5)
               minimiser->SetFixedVariable(j, names_jeans_params[j], stat_analysis.Value[j]);
            else
               minimiser->SetFixedVariable(j, names_jeans_params[j + 10], stat_analysis.Value[j]);
         }
      }
      // Add the other fixed parameters
      for (int j = 0; j < 10; j++)
         minimiser->SetFixedVariable(j + 9, names_jeans_params[j + 5], param[j + 5]);
      minimiser->SetFixedVariable(19, names_jeans_params[19], param[19]);

      // Minimize
      minimiser->Minimize();
      double_t chi2_min = minimiser->MinValue();
      if (is_verbose)
         cout << "Minimum: " << minimiser->MinValue()  << std::endl;
      minimiser->PrintResults();

      // Copy best-fit parameters and errors
      const double_t *xi = minimiser->X();
      //const Double_t *err = minimiser->Errors();
      double rhos = pow(10., xi[0]);
      double rs = pow(10., xi[1]);
      double alpha = xi[2];
      double beta = xi[3];
      double gamma = xi[4];
      double beta0 = xi[5];
      double betainf = xi[6];
      double ra = pow(10., xi[7]);
      double eta = xi[8];

      int dm_prof = param[5];
      int light_prof = param[12];
      int ani_prof = param[19];

      // Write parameters values in output file
      fprintf(fp, "%.*le %.*le %.*le %s %.*le %.*le %.*le %s %g %g %g %g %g %s %.*le %.*le %.*le %.*le %.*le\n",
              gSIM_SIGDIGITS, rhos, gSIM_SIGDIGITS, rs, gSIM_SIGDIGITS, stat_analysis.HaloSize,
              ("k" + (string)gNAMES_PROFILE[dm_prof]).c_str(),
              gSIM_SIGDIGITS, alpha, gSIM_SIGDIGITS, beta, gSIM_SIGDIGITS, gamma,
              ("k" + (string)gNAMES_LIGHTPROFILE[light_prof]).c_str(),
              param[7], param[8],
              param[9], param[10], param[11],
              ("k" + (string)gNAMES_ANISOTROPYPROFILE[ani_prof]).c_str(),
              gSIM_SIGDIGITS, beta0, gSIM_SIGDIGITS, betainf,
              gSIM_SIGDIGITS, ra, gSIM_SIGDIGITS, eta, gSIM_SIGDIGITS, chi2_min);
      fclose(fp);
   }
}
#endif

//______________________________________________________________________________
int main(int argc, char *argv [])
{

   // get the absolute path of executable:
   get_clumpy_dirs(string(argv[0]));

   bool is_arg = true;
   int choice = 0;

   if (argc <= 1 || (argc >= 2 && ((string)argv[1]).substr(0, 2) != "-r"))
      is_arg = false;
   else {
      // Search for selection
      string opt = argv[1];
      string ::size_type pos = opt.find_first_of("0123456789");
      string ::size_type pos_last = opt.find_last_of("0123456789");
      if (pos != string::npos)
         choice = atoi(opt.substr(pos, pos_last).c_str());

      // Print example values
      if (opt.find_first_of("D") != string::npos) {
         if (choice == 1)
            printf("   Jeans analysis with simple chi2 minimization:  %s -r1 %sdata/data_sigmap.txt output/chi2_binned.dat %sdata/params_jeans.txt 0.05\n", argv[0], gPATH_TO_CLUMPY_HOME.c_str(), gPATH_TO_CLUMPY_HOME.c_str());
         else if (choice == 2)
            printf("   Jeans analysis with bootstrap:  %s -r2 %sdata/data_vel.txt output/stat_bootstrap.dat %sdata/params_jeans.txt 0.05 100\n", argv[0], gPATH_TO_CLUMPY_HOME.c_str(), gPATH_TO_CLUMPY_HOME.c_str());
         printf("\n");

         return 1;
      }
   }

   // Usage of -1, -2, ..
   if (!is_arg || (choice <= 0 || choice > 2 || (choice == 1 && argc != 6))
         || (choice == 2 && argc != 7)) {
      printf("  usage:\n");
      printf("   [Chi2 minimization:        ]  %s -r1 data_file output_file param_file eps \n", argv[0]);
      printf("   [Bootstrap analysis:       ]  %s -r2 data_file output_file param_file eps Nbootstrap \n", argv[0]);
      printf("\n");
      printf(" N.B.: to see default parameter values (examples) and possible choices of dSphs, use \"-1D\", \"-2D\", etc.\n");
      printf("        -> data_file: file name (for sigma_p or velocity data)\n");
      printf("        -> output_file: output parameters\n");
      printf("        -> param_file: Jeans parameter file name\n");
      printf("        -> eps: precision\n");
      printf("        -> Nbootstrap: # of bootstrap samples\n");
      printf("\n\n");
      return 1;
   }


   string datafile = argv[2];
   string output = argv[3];
   string outdir = output.substr(0, output.find_last_of("\\/"));
   if (outdir != output) makedir(outdir.c_str());

   string param_file = argv[4];
   eps = atof(argv[5]);
   gSIM_SIGDIGITS = int(ceil(-log10(eps)));

   int n_bootstrap = 0;
   if (choice == 2)
      n_bootstrap = atoi(argv[6]);

   // Initialise parameters and data (fill stat_halo and halo_ref)
   halo_load_data4jeans(datafile, stat_halo, 0);
   halo_load_data4jeans(datafile, halo_ref, 0);
   load_jeansanalysis_parameters(param_file, stat_analysis);
   SetParameters();

   // Run the analysis
#if IS_ROOT
   Minimise_jeans(choice, n_bootstrap, output);
#else
   print_error("clumpy_jeansChi2.cc", "main()",
               "Jeans analysis minimization requires ROOT to be installed.");
#endif

   cout << "Statistical file saved in " << output << endl;

}

/*! \file clumpy_jeansChi2.cc
  \brief <b>Jeans analysis executable</b> (minimisation or bootstrap analysis)
*/
